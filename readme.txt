A generic genetic algorithms library, and an optimised version for the knapsack problem. Written for laboratory class scientific computing at Utrecht University.
