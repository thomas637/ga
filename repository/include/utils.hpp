#pragma once

#include "rng.hpp"
#include <string>

void printBits(std::uint64_t bitstring);

/* Counts the number of bits that are one in integer. */
int countOnes(std::uint64_t integer);

/* Returns the std::uint64_t the indexth chromosome is stored in. */
inline int binIndex(int index, int bitsPerChromosome)
{
    int chromosomesPerBin = 64 / bitsPerChromosome;
    return index / chromosomesPerBin;
}

/* Returns how many bits are to the left of the indexth chromosome. */
inline int leftSpace(int index, int bitsPerChromosome)
{
    int chromosomesPerBin = 64 / bitsPerChromosome;
    int numberOfChromosomesToTheLeft = index % chromosomesPerBin;
    return numberOfChromosomesToTheLeft * bitsPerChromosome;
}

/* Puts the 'index'th chromosome of population in the bin, as the 
   'position'th chromosome. Bin is an integer with zeroes on the place
   we insert the chromosome. */
inline void fillBin(int bitsPerChromosome, std::uint64_t* population, int index, 
    int position, std::uint64_t& bin)
{
    std::uint64_t chromosome = population[binIndex(index, bitsPerChromosome)];
    
    /* Kill irrelevant bits and end up with the bits in the left most position. */
    chromosome >>= (64 - (leftSpace(index, bitsPerChromosome) + bitsPerChromosome));
    chromosome <<= (64 - bitsPerChromosome);

    bin |= (chromosome >> (position * bitsPerChromosome)); 
}

/* Returns a bin with random chromosomes from the population. Takes the 
   state of an xorshift generator and updates the state! */
inline std::uint64_t randomBin(int bitsPerChromosome, std::uint64_t* population,
    std::uint64_t& state, int populationSize)
{
    int chromosomesPerBin = 64 / bitsPerChromosome;
    
    std::uint64_t bin = 0;

    int randomIndex;

    for (int i = 0; i < chromosomesPerBin; i++) {
        randomIndex = xorshiftDouble(state) * populationSize;        
        fillBin(bitsPerChromosome, population, randomIndex, i, bin);
    }

    return bin;
}

/* Returns the bits inside a chromosome as a string. */
std::string toString(std::uint64_t integer);

/* Prints your generation. */
void printChromosomes(struct Data generation, struct Parameters parameters);

std::uint64_t grayCodeToBinary(std::uint64_t bitString);

std::uint64_t binaryToGrayCode(std::uint64_t bitString);

std::uint32_t binaryToGrayCode(std::uint32_t bitString);
