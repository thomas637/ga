#pragma once

#include "knapsack.hpp"
#include <random>
#include <bsp.h>

std::uint64_t bspGeneticAlgorithm(struct KnapsackProblem problem,
     struct Parameters param, std::uint64_t seed, int flags);
