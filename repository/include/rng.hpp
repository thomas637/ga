/* Uses SIMD intrinsics via the VCL library by Agner Fog to
   generate 4 64 bit random integers at a time. */

#pragma once

#include <cstdint>
#include "../ext/FogSIMD/vectorclass.h" 

/* The 64 bit variant of Xorshift PRNG by Marsaglia, code from the paper
   ported to C++. By default chooses parameters recommended by Marsaglia. 
    
   Calculates 4 at a time using SIMD intrinsics via Agner Fog's VCL library.
   ('Ports' are trivial as we are talking about bit-operations.) */
inline Vec4uq xorshift(Vec4uq& state)
{
    state ^= (state << 13);
    state ^= (state >> 7);
    state ^= (state << 17);

    return state;
}

/* Non-simd version */
inline std::uint64_t xorshift(std::uint64_t& state)
{
    state ^= (state << 13);
    state ^= (state >> 7);
    state ^= (state << 17);

    return state;
}

/* Double in [0, 1] versions. */
//inline Vec xorshift(Vec4uq& state)

inline double xorshiftDouble(std::uint64_t& state)
{
    /* xorshift takes values in [0, 2^64 - 1], and the nearest representable
       double to 2^64 - 1 is 18446744073709551616.0. */
    return (double) xorshift(state) / 18446744073709551616.0;
}

/* For seeding the SIMD xorshift. */
Vec4uq seedWithPcg(std::uint64_t seed);
