#pragma once

#include <cstdlib>
#include <iostream>
#include <functional>
#include <algorithm>
#include <cstring>
#include "utils.hpp"

/* Flags determining the algorithm. */
enum flags {
    // Return the chromosome with (hopefully) good objective value.
    RETURN_VALUE = 0x00000001u,
    // Return the number of iterations until convergence.
    RETURN_ITERATIONS = 0x00000002u,
    // Use tournament selection with replacement.
    TOURNAMENT = 0x00000010u,
    // Use tournament selection without reselection.
    TOURNAMENT_NORESELECT = 0x00000020u, 
    // Use proportional selection with replacement.
    PROPORTIONAL = 0x00000030u,
    // Use proportional selection without replacement.
    PROPORTIONAL_NORESELECT = 0x00000040u,     
    // Always select the fittest individual and do not mutate it.
    ELITES = 0x00000100u,
    // Vanilla genetic algorithm.
    NOELITES = 0x00000200u,
    // Converge when insufficient progress is made (for unknown maximum objective value).
    UNKNOWN_CONVERGENCE = 0x00001000u,
    // Converge when the a priori known maximum is found.
    KNOWN_CONVERGENCE = 0x00002000u,
};

struct Data {
    /* Stores the chromosomes of a generation. Integers are just seen as a data structure
       of 64 bits, and referred to as bins to reflect this. We cram as many chromosomes
       into a bin as can fit whole. 

       So if the bit representation has 16 bits, that is 4 chromosomes as 16 * 4 = 64.
       If it has 33 bits, we only fit in one as 33 * 1 <= 64, but 33 * 2 > 64. */
    std::uint64_t* chromosomes;

    /* Stores the chromosomes advancing to the next generation. */
    std::uint64_t* survivors;

    /* Fittest chromosomes that survive no matter what and are not mutated. */
    std::uint64_t* elites;

    /* Stores the fitness values of the chromosomes. */
    double* fitnessValues;
};

struct Parameters {
    /* We mutate each bit with probability 2^-mutationProbability. */
    int mutationProbability;
    
    /* We stop when there is no improvement in this many iterations. */
    int convergence;

    /* Number of chromosomes in one generation. */
    int size; 

    /* One in this number survives to the next generation. */
    double survivorFraction;

    /* One in this number of survivors reproduces. */
    double parentFraction;

    /* Number of bits needed to represent one chromosome. */
    int bitsPerChromosome;

    /* Rng state. */
    std::uint64_t state;

    /* Rng state for SIMD. geneticAlgorithm initializes this. */
    Vec4uq stateVec;

    /* The a priori known maximum of the problem (you can leave this
       uninitialised if it is now known). */
    double maximum;

    /* Tournament size, can be left uninitialized if not used. */
    int tournamentSize;
};

/* The main algorithm. Returns a fit chromosome, or number
   of iterations until convergence, depending on flags. */
std::uint64_t geneticAlgorithm(
    std::function<double(std::uint64_t chromosome)> objectiveValueFunction,
    struct Parameters parameters, int flags);

/* Fills the fitness values with just the objective values. */
void calculateObjectives(struct Data data, struct Parameters param,
    std::function<double(std::uint64_t chromosome)> objectiveFunction);

/* Fills the fitness values with just the objective values. */
void calculateScaledObjectives(struct Data data, struct Parameters param,
    std::function<double(std::uint64_t chromosome)> objectiveFunction);

/* Generates survivors (with reselection) by taking 'tournamentSize' random individuals,
   and passing along the fittest. We assume numberOfSurvivors is a multiple of the number
   of chromosomes fitting into a bin. */
void selectTournamentReselection(struct Data data, int numberOfSurvivors,
    int tournamentSize, struct Parameters& param);

/* Generates survivors (with reselection) by taking 'tournamentSize' random individuals,
   and passing along the fittest. We assume numberOfSurvivors is a multiple of the number
   of chromosomes fitting into a bin. No reselection. */
void selectTournamentNoReselection(struct Data data, int numberOfSurvivors,
    int tournamentSize, struct Parameters& param);

/* Wheel selection, proportional to fitness. */
void selectProportionalReselection(struct Data data, struct Parameters &param,
                                   int numberOfSurvivors);

/* Wheel selection, proportional to fitness. No reselection. */
void selectProportionalNoReselection(struct Data data, struct Parameters &param,
                                   int numberOfSurvivors);

/* Flips the bits in an array bitstrings of length 'length' with
   probability 2^-n. Assumes 4 divides length. state is the state
   of the Xorshift which is used to introduce the randomness. */
void mutate(std::uint64_t* bitstrings, int n, int length, Vec4uq& state);

/* source is of length 'sourceNumber', and contains chromosomes of 
   'bitsPerChromosome' bits each. Fills destination with 'destNumber'
   bins filled with random chromosomes from source. Chromosomes from
   source may be selected twice. 'state' is for the rng. */
void randomFill(std::uint64_t* destination, std::uint64_t* source, 
    std::uint64_t& state, int destNumber, int sourceNumber, int bitsPerChromosome);

/* Fills chromosomes with the next generation. */ 
void nextGeneration(struct Data data, struct Parameters param, int flags);

struct Data dataCreator(struct Parameters param);

void destroyData(struct Data data);

/* Checks whether the parameters satisfy the assumptions. If not, 
   rounds the parameters so that it does, and prints a warning that it 
   has done so. */
void checkParameters(struct Parameters& param);

void printChromosomes(Data data, Parameters param);
