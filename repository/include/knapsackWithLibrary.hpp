#include "library.hpp"

/* Struct to hold a knapsack optimisation problem. We have
   `numberOfItems' items, prices[i] is the price of the ith item,
   weights[i] the weight of the ith item, and `weightLimit' is the
   maximum threshold of the combined weights of the items. */
struct KnapsackProblem {
    double* prices;
    double* weights;
    double weightLimit;
    int numberOfItems;
};

double objectiveValue(std::uint64_t item, struct KnapsackProblem& problem);

std::uint64_t knapsackGenetic(struct KnapsackProblem problem,
    struct Parameters param, int flags);
