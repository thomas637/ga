#include "library.hpp"

double eval(std::uint64_t bin)
{
    return (double) (bin >> 32);
}

/* To be implemented (should convert a gray coded binary
   number stored as the 32 left-most digits to its numerical value
   and cast to double. */
double evalGray(std::uint64_t bin)
{
    return (double) grayCodeToBinary((std::uint32_t) (bin >> 32));
}

std::uint64_t optimise(struct Parameters param, int flags)
{
    return geneticAlgorithm(&eval, param, flags);
}

std::uint64_t grayCodes(struct Parameters param, int flags)
{
    return geneticAlgorithm(&evalGray, param, flags);
}
