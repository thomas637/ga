#pragma once

#include "utils.hpp"
#include <cstring> // for memcpy
#include <algorithm>
#include <iostream>

/* Calculates the objective values of all items. The objective values
   are permuted however.
   
   If there are 3 knapsacks per bin, then the objective values of the 
   following knapsacks (one Vec4uq that gets fed into objectiveSIMD)
   
   0 1 2 | 3 4 5 | 6 7 8 | 9 10 11 
   
   are stored as
   
   0 3 6 9 | 2 4 7 10 | 2 5 8 11. */
void fill(struct KnapsackProblem problem, std::uint64_t* items, 
    double* objectiveValues, int bitsPerChromosome,
    int numberOfBins);

enum flags {
    // Return the chromosome with (hopefully) good objective value.
    RETURN_VALUE = 0x00000001u,
    // Return the number of iterations until convergence.
    RETURN_ITERATIONS = 0x00000002u,
    // Converge when insufficient progress is made (for unknown maximum objective value).
    UNKNOWN_CONVERGENCE = 0x00000010u,
    // Converge when the a priori known maximum is found.
    KNOWN_CONVERGENCE = 0x00000020u
};

/* Struct to hold a knapsack optimisation problem. We have
   `numberOfItems' items, prices[i] is the price of the ith item,
   weights[i] the weight of the ith item, and `weightLimit' is the
   maximum threshold of the combined weights of the items. */
struct KnapsackProblem {
    double* prices;
    double* weights;
    double weightLimit;
    int numberOfItems;
};

/* library.hpp with small adjustments to make use of SIMD for calculating
   the objective values. */
// ------------------------------------------------------------------------------------

struct Data {
    /* Stores the chromosomes of a generation. Integers are just seen as a data structure
       of 64 bits, and referred to as bins to reflect this. We cram as many chromosomes
       into a bin as can fit whole. 

       So if the bit representation has 16 bits, that is 4 chromosomes as 16 * 4 = 64.
       If it has 33 bits, we only fit in one as 33 * 1 <= 64, but 33 * 2 > 64. */
    std::uint64_t* chromosomes;

    /* Stores the chromosomes advancing to the next generation. */
    std::uint64_t* survivors;

    /* Fittest chromosomes that survive no matter what and are not mutated. */
    std::uint64_t elites;

    /* Stores the fitness values of the chromosomes. */
    double* fitnessValues;
};

struct Parameters {
    /* We mutate each bit with probability 2^-mutationProbability. */
    int mutationProbability;
    
    /* We stop when there is no improvement in this many iterations. */
    int convergence;

    /* Number of chromosomes in one generation. */
    int size; 

    /* One in this number survives to the next generation. */
    double survivorFraction;

    /* One in this number of survivors reproduces. */
    double parentFraction;

    /* Number of bits needed to represent one chromosome. */
    int bitsPerChromosome;

    /* Rng state. */
    std::uint64_t state;

    /* Rng state for SIMD. geneticAlgorithm initializes this. */
    Vec4uq stateVec;

    /* The a priori known maximum of the problem (you can leave this
       uninitialised if it is now known). */
    double maximum;
};

/* The main algorithm. Returns a fit chromosome, or number
   of iterations until convergence, depending on flags. */
std::uint64_t knapsackGenetic(struct KnapsackProblem problem,
    struct Parameters parameters, int flags);

/* Same as above, but preserves the chromosomes of the last iteration.
   and allows for arbitrary initialization. */
std::uint64_t knapsackGenetic(struct KnapsackProblem problem,
    struct Parameters param, int flags, struct Data data);

/* The knapsack is stored as the left-most bits. */
double objectiveValue(std::uint64_t knapsack, struct KnapsackProblem problem);

/* Generates survivors (with reselection) by taking 'tournamentSize' random individuals,
   and passing along the fittest. We assume numberOfSurvivors is a multiple of the number
   of chromosomes fitting into a bin. */
void selectTournament(struct Data data, int numberOfSurvivors,
    int tournamentSize, struct Parameters& param);

/* Flips the bits in an array bitstrings of length 'length' with
   probability 2^-n. Assumes 4 divides length. state is the state
   of the Xorshift which is used to introduce the randomness. */
void mutate(std::uint64_t* bitstrings, int n, int length, Vec4uq& state);

/* source is of length 'sourceNumber', and contains chromosomes of 
   'bitsPerChromosome' bits each. Fills destination with 'destNumber'
   bins filled with random chromosomes from source. Chromosomes from
   source may be selected twice. 'state' is for the rng. */
void randomFill(std::uint64_t* destination, std::uint64_t* source, 
    std::uint64_t& state, int destNumber, int sourceNumber, int bitsPerChromosome);

/* Fills chromosomes with the next generation. */ 
void nextGeneration(struct Data data, struct Parameters param, int flags);

/* Returns the index of the chromosome corresponding to fitnessValues[fitnessIndex]. */
inline int permutedIndex(int fitnessIndex, int bitsPerChromosome)
{
    int chromosomesPerBin = 64 / bitsPerChromosome;
    
    /* We calculate the objective values in chunks of 4 * chromosomesPerBin. */
    int indexRounded = fitnessIndex / (4 * chromosomesPerBin); 
    
    /* Each four bins get permuted the same way, so this determines the position within the
       four bins. */
    int indexRemainder = fitnessIndex % (4 * chromosomesPerBin);
 
    return indexRounded * 4 * chromosomesPerBin + indexRemainder / 4 + 
        (indexRemainder % 4) * chromosomesPerBin;
}

struct Data dataCreator(struct Parameters param);

void destroyData(struct Data data);

/* Checks whether the parameters satisfy the assumptions. If not, 
   rounds the parameters so that it does, and prints a warning that it 
   has done so. */
void checkParameters(struct Parameters& param);
