#include "../include/integerProblem.hpp"
#include <fstream>
#include <random>

/* Compares the number of iterations for 32-bit integer maximisation
   for binary vs graycodes and various 
   mutation probabilities. Averaged over numberOfRuns runs. */
void fileMaker(std::ofstream& output, int numberOfRuns, int minProbability,
    int maxProbability, int flags, struct Parameters param)
{
    output << "mutation probability,binary,gray codes" << std::endl;

    std::mt19937 rng;
    rng.seed(param.state);
    std::uniform_int_distribution<std::uint64_t> seedGenerator;

    for (int probability = minProbability; probability <= maxProbability; probability++) {
        param.mutationProbability = probability;

        std::uint64_t binaryRuns = 0;
        std::uint64_t grayRuns = 0;

        for (int i = 0; i < numberOfRuns; i++) {
            binaryRuns += optimise(param, flags);
            grayRuns += grayCodes(param, flags); 
            param.state = seedGenerator(rng);
        }

        output << 1.0 / std::pow(2, param.mutationProbability) << "," << binaryRuns / numberOfRuns 
            << "," << grayRuns / numberOfRuns << std::endl;  
    }
}


int main()
{
    struct Parameters param;
    param.size = 4096;
    param.survivorFraction = 64;
    param.parentFraction = 1;
    param.bitsPerChromosome = 32;
    param.state = 20923490325u;
    param.tournamentSize = 2;
    param.maximum = (double) 0xFFFFFFFFu;

    std::ofstream output;
    output.open("../results/integerResults.csv");
    
    fileMaker(output, 30, 3, 10, RETURN_ITERATIONS | TOURNAMENT | ELITES | KNOWN_CONVERGENCE, param);
    
    output.close();
}
