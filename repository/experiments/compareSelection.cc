#include "../include/knapsackWithLibrary.hpp"
#include "../test/include/problemGenerator.hpp"
#include <random>
#include <fstream>

/* Responsible for creating a single line of the csv, corresponding to the
   parameters specified by param. Flags is everything except for the 
   elites / noelites flag. */
void singleRun(struct Parameters param, std::ofstream& output, 
    struct KnapsackProblem problem, int flags, int numberOfSamples)
{
    checkParameters(param);

    output << param.mutationProbability << "," << param.size << 
        "," << param.survivorFraction << "," << param.parentFraction <<
        ","; 

    std::mt19937 rng;
    rng.seed(param.state);
    std::uniform_int_distribution<std::uint64_t> seedGenerator;

    for (int i = 0; i < numberOfSamples; i++) {
        output << param.size * knapsackGenetic(problem, param, flags | TOURNAMENT) << ",";
        param.state = seedGenerator(rng);
    }

    output << "T,Y" << std::endl;

    output << param.mutationProbability << "," << param.size << 
        "," << param.survivorFraction << "," << param.parentFraction <<
        ","; 

    for (int i = 0; i < numberOfSamples; i++) {
        output << param.size * knapsackGenetic(problem, param, flags | TOURNAMENT_NORESELECT) << ",";
        param.state = seedGenerator(rng);
    }

    output << "T,N" << std::endl;

    output << param.mutationProbability << "," << param.size << 
        "," << param.survivorFraction << "," << param.parentFraction <<
        ","; 

    for (int i = 0; i < numberOfSamples; i++) {
        output << param.size * knapsackGenetic(problem, param, flags | PROPORTIONAL) << ",";
        param.state = seedGenerator(rng);
    }

    output << "FP,Y" << std::endl;

    output << param.mutationProbability << "," << param.size << 
        "," << param.survivorFraction << "," << param.parentFraction <<
        ","; 

    for (int i = 0; i < numberOfSamples; i++) {
        output << param.size * knapsackGenetic(problem, param, flags | PROPORTIONAL_NORESELECT) << ",";
        param.state = seedGenerator(rng);
    }

    output << "FP,N" << std::endl;
}

/* Takes one commandline argument, the number of runs n. This program makes a csv file
   with lines containing

   mutation probability, population size, survivor fraction, parent fraction, 

   and then n columns with the number of iterations until convergence multiplied by
   population size for different seeds, for each of the four selection methods. */
int main(int argc, char** argv)
{
    int numberOfSamples = atoi(argv[1]);

    int minMutation = 3;
    int maxMutation = 8; 
    int minGeneration = 2048;
    int maxGeneration = 16 * 2048;
    int minSurvivor = 32;
    int maxSurvivor = 256;
    int maxParent = 1;

    struct Parameters param;
    param.bitsPerChromosome = 20;
    param.state = 8983498589345;
    param.maximum = param.bitsPerChromosome - 1;
    param.convergence = 1000;
    param.tournamentSize = 2;

    struct KnapsackProblem problem = smallProblem(param.bitsPerChromosome);
    
    int flags = ELITES | RETURN_ITERATIONS | KNOWN_CONVERGENCE;

    std::ofstream output;
    output.open("../results/compareSelection.csv");

    output << "mutation probability,generation size,survivor fraction,parent fraction,";

    for (int i = 0; i < numberOfSamples; i++) {
        output << "generation size multiplied by iterations until convergence" << i + 1 << ",";
    }

    output << "selection (T/FP),replacement (Y/N)" << std::endl;

    for (int mutation = minMutation; mutation <= maxMutation; mutation++) {
        for (int generation = minGeneration; generation <= maxGeneration; generation *= 2) {
            for (int survivor = minSurvivor; survivor <= maxSurvivor; survivor *= 2) {
                for (int parent = 1; parent <= maxParent; parent *= 2) {
                    param.mutationProbability = mutation;
                    param.size = generation;
                    param.survivorFraction = survivor;
                    param.parentFraction = parent;

                    singleRun(param, output, problem, flags, numberOfSamples);
                }
            }
        }
    }

    output.close();

    destroyProblem(problem);
}
