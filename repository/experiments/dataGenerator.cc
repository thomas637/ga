#include "../include/knapsack.hpp"
#include "../test/include/problemGenerator.hpp"
#include <random>
#include <fstream>

/* Responsible for creating a single line of the csv, corresponding to the
   parameters specified by param. */
void singleRun(struct Parameters param, std::ofstream& output, 
    struct KnapsackProblem problem, int flags, int numberOfSamples)
{
    output << param.mutationProbability << "," << param.size << 
        "," << param.survivorFraction << "," << param.parentFraction <<
        ","; 

    std::mt19937 rng;
    rng.seed(param.state);
    std::uniform_int_distribution<std::uint64_t> seedGenerator;
    for (int i = 0; i < numberOfSamples; i++) {
        output << param.size * knapsackGenetic(problem, param, flags) << ",";
        param.state = seedGenerator(rng);
    }

    output << std::endl;
}

/* Takes one commandline argument, the number of runs n. This program makes a csv file
   with lines containing

   mutation probability, population size, survivor fraction, parent fraction, 

   and then n columns with the number of iterations until convergence multiplied by,
   population size for different seeds. The algorithm is no elites, tournament selection
   with replacement, and tournament size 2. */
int main(int argc, char** argv)
{
    int numberOfSamples = atoi(argv[1]);

    /* All powers of two. */
    int minMutation = 1;
    int maxMutation = 8; 
    int minGeneration = 2048;
    int maxGeneration = 16 * 2048;
    int minSurvivor = 16;
    int maxSurvivor = 256;
    int maxParent = 1;

    struct Parameters param;
    param.bitsPerChromosome = 24;
    param.state = 8983498589345;
    param.maximum = 13549094;
    param.convergence = 1000;

    struct KnapsackProblem problem = internetProblem();
    
    int flags = KNOWN_CONVERGENCE | RETURN_ITERATIONS;

    std::ofstream output;
    output.open("../results/data.csv");

    output << "mutation probability,generation size,survivor fraction,parent fraction,";

    for (int i = 0; i < numberOfSamples; i++) {
        output << "generation size multiplied by iterations until convergence " << i + 1 << ",";
    }

    output << std::endl;

    for (int mutation = minMutation; mutation <= maxMutation; mutation++) {
        for (int generation = minGeneration; generation <= maxGeneration; generation *= 2) {
            for (int survivor = minSurvivor; survivor <= maxSurvivor; survivor *= 2) {
                for (int parent = 1; parent <= maxParent; parent *= 2) {
                    param.mutationProbability = mutation;
                    param.size = generation;
                    param.survivorFraction = survivor;
                    param.parentFraction = parent;

                    singleRun(param, output, problem, flags, numberOfSamples);
                }
            }
        }
    }

    output.close();

    destroyProblem(problem);
}
