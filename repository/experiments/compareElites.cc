#include "../include/knapsackWithLibrary.hpp"
#include "../test/include/problemGenerator.hpp"
#include <random>
#include <fstream>

/* Responsible for creating a single line of the csv, corresponding to the
   parameters specified by param. Flags is everything except for the 
   elites / noelites flag. */
void singleRun(struct Parameters param, std::ofstream& output, 
    struct KnapsackProblem problem, int flags, int numberOfSamples)
{
    checkParameters(param);

    output << param.mutationProbability << "," << param.size << 
        "," << param.survivorFraction << "," << param.parentFraction <<
        ","; 

    std::mt19937 rng;
    rng.seed(param.state);
    std::uniform_int_distribution<std::uint64_t> seedGenerator;
    for (int i = 0; i < numberOfSamples; i++) {
        output << param.size * knapsackGenetic(problem, param, flags | ELITES) << ",";
        param.state = seedGenerator(rng);
    }

    output << "1" << std::endl;

    output << param.mutationProbability << "," << param.size << 
        "," << param.survivorFraction << "," << param.parentFraction <<
        ","; 

    for (int i = 0; i < numberOfSamples; i++) {
        output << param.size * knapsackGenetic(problem, param, flags | NOELITES) << ",";
        param.state = seedGenerator(rng);
    }

    output << "0" << std::endl;
}

/* Takes one commandline argument, the number of runs n. This program makes a csv file
   with lines containing

   mutation probability, population size, survivor fraction, parent fraction, 

   and then n columns with the number of iterations until convergence multiplied by
   population size for different seeds and elites + tournament selection, no replacement.
   Then another n columns with th enumber of iterations until convergence multiplied by 
   population size for different seeds with no elites + tournament selection, no replacement. */
int main(int argc, char** argv)
{
    int numberOfSamples = atoi(argv[1]);

    /* All powers of two. */
    int minMutation = 3;
    int maxMutation = 6; 
    int minGeneration = 4096;
    int maxGeneration = 4 * 4096;
    int minSurvivor = 64;
    int maxSurvivor = 256;
    int maxParent = 1;

    struct Parameters param;
    param.bitsPerChromosome = 24;
    param.state = 8983498589345;
    param.maximum = param.bitsPerChromosome - 1;
    param.convergence = 1000;
    param.tournamentSize = 2;

    struct KnapsackProblem problem = smallProblem(param.bitsPerChromosome);
    
    int flags = TOURNAMENT | RETURN_ITERATIONS | KNOWN_CONVERGENCE;

    std::ofstream output;
    output.open("../results/compareElites.csv");

    output << "mutation probability,generation size,survivor fraction,parent fraction,";

    for (int i = 0; i < numberOfSamples; i++) {
        output << "generation size multiplied by iterations until convergence elites " << i + 1 << ",";
    }

    output << "elites (1/0)" << std::endl;

    for (int mutation = minMutation; mutation <= maxMutation; mutation++) {
        for (int generation = minGeneration; generation <= maxGeneration; generation *= 2) {
            for (int survivor = minSurvivor; survivor <= maxSurvivor; survivor *= 2) {
                for (int parent = 1; parent <= maxParent; parent *= 2) {
                    param.mutationProbability = mutation;
                    param.size = generation;
                    param.survivorFraction = survivor;
                    param.parentFraction = parent;

                    singleRun(param, output, problem, flags, numberOfSamples);
                }
            }
        }
    }

    output.close();

    destroyProblem(problem);
}
