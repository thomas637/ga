#include "../include/knapsack.hpp"
#include <chrono>
#include <iostream>
#include <list>
#include <vector>

#define NITERS 10000
#define NITERSSLOW 1000

void fillSlow(double* prices, double* weights, double weightLimit,
    std::vector<std::list<char>> sacks, double* objectiveValues,
    int numberOfSacks, int numberOfItems)
{
    for (int i = 0; i < numberOfSacks; i++) {
        double price = 0;
        double weight = 0;
    
        int j = 0;
        for (auto item = (sacks[i]).begin(); item != (sacks[i]).end(); item++) {
            price += *item * prices[j];
            weight += *item * weights[j];
            j++;
        }
        
        if (weight <= weightLimit) {
            objectiveValues[i] = price;
        } else {
            objectiveValues[i] = 0;
        }
    } 
}

void timeSlow()
{
    /* Create knapsacks of 32 items each. */
    int noOfSacks = 4096;
    double* results = new double[noOfSacks];
    double* prices = new double[32];
    double* weights = new double[32];

    std::vector<std::list<char>> knapsacks(noOfSacks); 

    /* Every knapsack contains all items. */
    for (int i = 0; i < noOfSacks; i++) {
        for (int j = 0; j < 32; j++) {
            knapsacks[i].push_back(1);
        }
    }

    /* Weight and price is one. */
    for (int i = 0; i < 32; i++) {
        prices[i] = 1;
        weights[i] = 1;
    }

    std::chrono::high_resolution_clock::time_point time1 = 
        std::chrono::high_resolution_clock::now();

    /* Weight limit is high enough, so all knapsacks should have objective value 32. */
    for (int i = 0; i < NITERSSLOW; i++) {
        fillSlow(prices, weights, 200.0, knapsacks, results, 32, noOfSacks);
    }

    std::chrono::high_resolution_clock::time_point time2 = 
        std::chrono::high_resolution_clock::now();

    std::chrono::duration<double> time_span = 
        std::chrono::duration_cast<std::chrono::duration<double>>(time2 - time1);

    std::cout << "Calculating the objective value of a knapsack carrying 32 items took "
     << time_span.count() * 1000000000 / NITERS / noOfSacks << " ns. on average. " << std::endl;

    delete[] results;
    delete[] prices;
    delete[] weights; 
}

void timeOptimised()
{
    /* Create knapsacks of 32 items each. */
    int noOfSacks = 4096;
    std::uint64_t* knapsacks = new std::uint64_t[noOfSacks / 2];

    struct KnapsackProblem problem;
    double* results = new double[noOfSacks];
    problem.prices = new double[32];
    problem.weights = new double[32];
    problem.numberOfItems = 32;
    problem.weightLimit = 200;

    /* Every knapsack contains all items. */
    for (int i = 0; i < noOfSacks / 2; i++) {
        knapsacks[i] = 0xFFFFFFFFFFFFFFFFu;
    }

    /* Weight and price is one. */
    for (int i = 0; i < 32; i++) {
        problem.prices[i] = 1;
        problem.weights[i] = 1;
    }

    std::chrono::high_resolution_clock::time_point time1 = 
        std::chrono::high_resolution_clock::now();

    /* Weight limit is high enough, so all knapsacks should have objective value 32. */
    for (int i = 0; i < NITERS; i++) {
        fill(problem, knapsacks, results, 32, noOfSacks / 2);
    }

    std::chrono::high_resolution_clock::time_point time2 = 
        std::chrono::high_resolution_clock::now();

    std::chrono::duration<double> time_span = 
        std::chrono::duration_cast<std::chrono::duration<double>>(time2 - time1);

    std::cout << "Optimised version calculating the objective value of a knapsack carrying 32 items took "
     << time_span.count() * 1000000000 / NITERS / noOfSacks <<
     " ns. on average with optimisations. " << std::endl;

    delete[] knapsacks;
    delete[] results;
    delete[] problem.prices;
    delete[] problem.weights; 
}

int main()
{
    timeSlow();
    timeOptimised();
}
