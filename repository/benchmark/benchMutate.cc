#include "../src/library.cc"
#include "../src/utils.cc"
#include <iostream>
#include <chrono>
#include <fstream>

// galib stuff
#include<sstream>
#include<string>
#include<list>
#include<cmath>
#include<cstdlib>
#include <random>
#include <vector>

std::random_device rd; // NOLINT(cert-err58-cpp)
std::default_random_engine engine(rd()); // NOLINT(cert-err58-cpp)

/**
 * Produces a real number randomly between 0.0 and 1.0.
 */
double random_real(double a=0.0, double b=1.0) {
    auto distribution = std::uniform_real_distribution<double>(a, b);
    return distribution(engine);
}

void mutateSlow(std::vector<std::list<char>> bitstrings, double probability, int totalChromosomes,
    int bitsPerChromosome) {
    for (int i = 0; i < totalChromosomes; i++) {
        for (auto bit = bitstrings[i].begin(); bit != bitstrings[i].end(); bit++) {
            if (random_real() <= probability) {
                *bit = 1 - *bit;
            }
        }
    }
}

/* Mutates a small array many times. Returns the average time to mutate one bit in ns. */
double timingsSmallSlow(double probability)
{
    /* 1024 bytes in total, let's assume 32 bits per sack (not that that matters). */
    int noOfSacks = 1024 * 8 / 32; 
    std::vector<std::list<char>> knapsacks(noOfSacks); 

    /* Every knapsack contains all items. */
    for (int i = 0; i < noOfSacks; i++) {
        for (int j = 0; j < 32; j++) {
            knapsacks[i].push_back(1);
        }
    }

    int NITERS = 1024;    

    std::chrono::high_resolution_clock::time_point time1 = 
        std::chrono::high_resolution_clock::now();
    
    for (int i = 0; i < NITERS; i++) {
        mutateSlow(knapsacks, probability, noOfSacks, 32);
    }

    std::chrono::high_resolution_clock::time_point time2 = 
        std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_span = 
        std::chrono::duration_cast<std::chrono::duration<double>>(time2 - time1);

    return time_span.count() * 1000000000 / (noOfSacks * 32 * NITERS); 
}

/* Mutates a small array many times. Returns the average time to mutate one bit in ns. */
double timingsSmall(int n)
{
    Vec4uq state = seedWithPcg(34959034590345);

    /* One kilibyte */
    std::uint64_t totalBits = (std::uint64_t) 1024 * 8;
    std::uint64_t* bitstrings = new std::uint64_t[totalBits / 64];

    for (unsigned int i = 0; i < totalBits / 64; i++) {
        bitstrings[i] = 0;
    }

    int NITERS = 1024 * 1024;    

    std::chrono::high_resolution_clock::time_point time1 = 
        std::chrono::high_resolution_clock::now();
    
    for (int i = 0; i < NITERS; i++) {
        mutate(bitstrings, n, totalBits / 64, state);
    }

    std::chrono::high_resolution_clock::time_point time2 = 
        std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_span = 
        std::chrono::duration_cast<std::chrono::duration<double>>(time2 - time1);

    delete[] bitstrings;

    return time_span.count() * 1000000000 / (totalBits * NITERS); 
}

int main()
{
    std::ofstream timingsFile;

    timingsFile.open("../results/mutateTimings.txt");

    timingsFile << "n,optimised,slow" << std::endl;

    for (int n = 1; n <= 10; n++) {
        //timings(timingsFile, n);
        timingsFile << n << "," << timingsSmall(n) << ",";
        timingsFile << timingsSmallSlow(1.0 / (1 << n)) << std::endl;
    }

    timingsFile.close();
}
