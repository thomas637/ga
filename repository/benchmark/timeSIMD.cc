#include "../include/rng.hpp"
#include <chrono>
#include <iostream>

#define NITERS 1000000000

int main()
{
    Vec4uq state = seedWithPcg(1934034509);
    std::chrono::high_resolution_clock::time_point time1 = 
        std::chrono::high_resolution_clock::now();

    
    time1 = std::chrono::high_resolution_clock::now();

    for (int i = 0; i < NITERS; i++) {
        xorshift(state);
    }

    std::chrono::high_resolution_clock::time_point time2 = 
        std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_span = 
        std::chrono::duration_cast<std::chrono::duration<double>>(time2 - time1);

    std::cout << "Average time to produce 256 bits: " << time_span.count() * 1000000000 / NITERS << " ns." << std::endl;
    std::cout << "Here is a random number so the compiler does not optimise the latter loop away: " << state[0] << std::endl;
}
