#include "../include/knapsack.hpp"
#include "../test/include/problemGenerator.hpp"
#include <chrono>
#include <fstream>
#include <iostream>

#define NITERS 100

std::uint64_t benchUnknown()
{
    struct Parameters param;
    param.mutationProbability = 4;
    param.convergence = 1000;
    param.size = 4096; // with the larger population size of 32768, it actually does not find the maximum.
    param.survivorFraction = 2;
    param.parentFraction = 1;
    param.bitsPerChromosome = 24;
    param.state = 2904390324;
    param.stateVec = seedWithPcg(293920344);

    struct KnapsackProblem problem = internetProblem();
   
    std::uint64_t argmax = knapsackGenetic(problem, 
       param, RETURN_VALUE | UNKNOWN_CONVERGENCE);

    destroyProblem(problem);

    return argmax;
}


std::uint64_t benchKnown()
{
    struct Parameters param;
    param.mutationProbability = 4;
    param.size = 4096; // with the larger population size of 32768, it actually does not find the maximum.
    param.survivorFraction = 2;
    param.parentFraction = 1;
    param.bitsPerChromosome = 24;
    param.state = 2904390324;
    param.stateVec = seedWithPcg(293920344);
    param.maximum = 13549094;

    struct KnapsackProblem problem = internetProblem();
    
    std::uint64_t iterations = knapsackGenetic(problem, param,
        RETURN_ITERATIONS | KNOWN_CONVERGENCE);

    destroyProblem(problem);

    return iterations;
}

int main()
{
    std::cout << benchUnknown() << std::endl;
}
