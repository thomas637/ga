#include "../include/rng.hpp"
#include <iostream>

/* PCG_RXS_M_XS generator from O'Neill. */
std::uint64_t pcgAdvance(std::uint64_t& state) 
{
    int shift = state >> 59;
    state ^= state >> (5 + shift);
    state *= 12605985483714917081u;

    return state ^ (state >> 43);
}


Vec4uq seedWithPcg(std::uint64_t seed)
{
    Vec4uq packedSeed;

    for (int i = 0; i < 4; i++) {
        packedSeed.insert(i, pcgAdvance(seed));
    }    
    
    return packedSeed;
}
