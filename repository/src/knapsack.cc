#include "../include/knapsack.hpp"

/* Takes a reference to four packed bins 'items'. The calculates the 
   objective value of the left-most knapsack in each of those bins, and 
   returns the result as four packed doubles. The left-most knapsack of 
   each of those bins is destroyed, and the second left-most knapsack is
   now the left-most knapsack. */
inline Vec4d objectiveSIMD(struct KnapsackProblem problem, Vec4uq& items)
{
    Vec4d value = 0;
    Vec4d weight = 0;

    for (int i = 0; i < problem.numberOfItems; i++) {
        // add prices[i] and weights[i] iff the left-most bit of items is 1
        value = if_add((items & 0x8000000000000000u) > 0, value, problem.prices[i]);
        weight = if_add((items & 0x8000000000000000u) > 0, weight, problem.weights[i]);
        items <<= 1u;
    }

    return select(weight <= problem.weightLimit, value, 0);
}

void fill(struct KnapsackProblem problem, std::uint64_t* items, 
    double* objectiveValues, int bitsPerChromosome,
    int numberOfBins)
{
    int chromosomesPerBin = 64 / bitsPerChromosome;
    
    Vec4uq item; // to store the knapsacks.
    Vec4d result; // to store the objective values.

    int objectiveCounter = 0; // index for 'objectiveValues'.

    /* Assume we have a multiple of four bins. Rounding
       up to a multiple of four does not take any longer
       when everything is SIMD'd, and we do not have to 
       distinguish cases. */
    for (int i = 0; i < numberOfBins; i += 4) {       
        item.load(items + i);

        for (int j = 0; j < chromosomesPerBin; j++) {
            result = objectiveSIMD(problem, item);
            result.store(objectiveValues + objectiveCounter);
            objectiveCounter += 4; // the previous line stores 4 doubles.
        }
    }
}

/* library.cc with small adjustments to make use of SIMD for calculating
   the objective values. */
// ------------------------------------------------------------------------------------

std::uint64_t knapsackGenetic(struct KnapsackProblem problem,
    struct Parameters param, int flags)
{
    checkParameters(param);

    struct Data data = dataCreator(param);

    std::uint64_t result = knapsackGenetic(problem, param, flags, data);
    
    destroyData(data);

    return result;
}

/* Generates survivors (with reselection) by taking 'tournamentSize' random individuals,
   and passing along the fittest. We assume numberOfSurvivors is a multiple of the number
   of chromosomes fitting into a bin. */
void selectTournament(struct Data data, int numberOfSurvivors,
    int tournamentSize, struct Parameters& param)
{
    int chromosomesPerBin = 64 / param.bitsPerChromosome;

    /* In each of these iterations, we fill one bin. */
    for (int i = 0; i < numberOfSurvivors; i+= chromosomesPerBin) {

        /* We fill this bin this loop, start empty, so all zeroes. */
        std::uint64_t bin = 0;
        for (int j = 0; j < chromosomesPerBin; j++) {

            /* Start of tournament for the jth spot. */
            int randomIndex = xorshiftDouble(param.state) * param.size;

            double max = data.fitnessValues[randomIndex];
            double argmax = permutedIndex(randomIndex, param.bitsPerChromosome);

            /* Check whether the next participant beats the previous one. */
            for (int j = 1; j < tournamentSize; j++) {
                randomIndex = xorshiftDouble(param.state) * param.size;

                if (data.fitnessValues[randomIndex] > max) {
                    max = data.fitnessValues[randomIndex];
                    argmax = permutedIndex(randomIndex, param.bitsPerChromosome);
                }
            }

            /* End of the tournament for the jth spot, won by the chromosome
               in position argmax. */
            fillBin(param.bitsPerChromosome, data.chromosomes, 
                permutedIndex(argmax, param.bitsPerChromosome), j, bin);
        }
       
        /* i is the chromosome index, so we divide to get the bin index. */        
        data.survivors[i / chromosomesPerBin] = bin;
    }
}

/* Overwrites mothers1 and mothers2 with their uniformly crossover'd children.  */
inline void crossover(Vec4uq& mother1, Vec4uq& mother2, Vec4uq& state)
{
    Vec4uq mask = xorshift(state); 

    Vec4uq child1; 
    child1 = (mother1 & mask) | (mother2 & ~mask);

    mother2 = (mother1 & ~mask) | (mother2 & mask);

    mother1 = child1;
}

/* Flips the bits in an array bitstrings of 'length' bins with
   probability 2^-n. Assumes 4 divides length. state is the state
   of the Xorshift which is used to introduce the randomness. */
void mutate(std::uint64_t* bitstrings, int n, int length, Vec4uq& state)
{
    Vec4uq packedInts;

    for (int i = 0; i < length; i+= 4) {
        packedInts.load(bitstrings + i); // The bits we flip this iteration.

        /* x and y = 1 iff x and y are one, so P(x and y = 1) 
           is equal to P(x = 1) P(x = y) if x and y are independent. 
           So when we bitwise-and n uniform random integers, the bits
           are one with probability 2^-n. */
        Vec4uq bitMask = xorshift(state);
        for (int j = 1; j < n; j++) {
            bitMask &= xorshift(state);
        }

        /* 0 xor 0 = 0, 1 xor 0 = 1, 0 xor 1 = 1, 1 xor 1 = 0, so
           xoring a bit with 1 flips it, xoring with 0 does nothing. */
        packedInts ^= bitMask;
        packedInts.store(bitstrings + i);
    }
}

/* Uniform random crossover of all chromosomes in
   parents[i] with the chromosomes in parents[i + 4]. 
   Assumes numberOfParentBins is a multiple of 8. */
void crossoverInPlace(std::uint64_t* parents, int numberOfParentBins, Vec4uq& state)
{
    Vec4uq mothers1, mothers2;
    for (int i = 0; i < numberOfParentBins; i += 8) {
        mothers1.load(parents + i);
        mothers2.load(parents + i + 4);

        crossover(mothers1, mothers2, state);

        mothers1.store(parents + i);
        mothers2.store(parents + i + 4);
    }
}

void nextGeneration(struct Data data, struct Parameters param, int flags)
{
    int chromosomesPerBin = 64 / param.bitsPerChromosome;

    /* Round so it fits neatly into bins. */
    int numberOfBins = param.size / chromosomesPerBin;
    int survivorBins = numberOfBins / param.survivorFraction;

    selectTournament(data, survivorBins * chromosomesPerBin, 2, param);

    /* The survivors make up the first part of the new generation. */
    memcpy(data.survivors, data.chromosomes, 
     survivorBins * sizeof(std::uint64_t));

    /* As the order the survivors are selected in, is arbitrary, taking the first 1/nth part,
       means we select survivors with probability 1/n to be selected for reproduction. */

    /* Fill the remaining space of 'chromosomes' with randomly selected couples. */
    for (int i = survivorBins; i < numberOfBins; i++) {
        data.chromosomes[i] = randomBin(param.bitsPerChromosome, data.survivors, param.state, 
         survivorBins * chromosomesPerBin / param.parentFraction); 
    }
    
    crossoverInPlace(data.chromosomes + survivorBins, numberOfBins - survivorBins, param.stateVec);
}

double objectiveValue(std::uint64_t knapsack, struct KnapsackProblem problem)
{
    double price = 0;
    double weight = 0;

    for (int i = 0; i < problem.numberOfItems; i++) {
        if ((knapsack & 0x8000000000000000u) > 0) {
            price += problem.prices[i];
            weight += problem.weights[i];
        }
        knapsack <<= 1;
    }

    if (weight <= problem.weightLimit) {
        return price;
    } else {
        return 0;
    }
}

/* Same as the normal version, but the generation is preserved. */
// ----------------------------------------------------------------------------------------------------
std::uint64_t knapsackGenetic(struct KnapsackProblem problem,
    struct Parameters param, int flags, struct Data data)
{
    /* Data allocation and initalization. */
    // ----------------------------------------------------------------------------------------
    param.stateVec = seedWithPcg(param.state);

    int chromosomesPerBin = 64 / param.bitsPerChromosome; 

    /* Random initialisation. */
    for (int i = 0; i < param.size / chromosomesPerBin; i+= 4) {
        (param.stateVec).store(data.chromosomes + i);
        xorshift(param.stateVec);
    } 

    /* The main loop of the algorithm. */
    // ----------------------------------------------------------------------------------------

    std::uint64_t loopCounter = 0;
    double maxSoFar; // The maximum objective value in all iterations so far.
    int maxIterationSoFar = 0; // The iteration this was achieved.
    std::uint64_t argmaxSoFar = 0; // The maximiser.
    bool keepGoing = true;

    while (keepGoing) {
        fill(problem, data.chromosomes, data.fitnessValues,
            param.bitsPerChromosome, param.size / chromosomesPerBin);      

        auto maxIterator = std::max_element(data.fitnessValues, data.fitnessValues + param.size);
        int maxIndex = std::distance(data.fitnessValues, maxIterator);
        double max = *maxIterator;
        
        if (loopCounter == 0) {
            maxSoFar = max;
            fillBin(param.bitsPerChromosome, data.chromosomes, maxIndex, 0, argmaxSoFar);
        }

        std::uint64_t argmax = 0;
        fillBin(param.bitsPerChromosome, data.chromosomes, maxIndex, 0, argmax);
        data.elites = argmax;

        if (max > maxSoFar) {
            maxSoFar = max;
            maxIterationSoFar = loopCounter;

            argmaxSoFar = 0;
            fillBin(param.bitsPerChromosome, data.chromosomes, 
                permutedIndex(maxIndex, param.bitsPerChromosome), 0, argmaxSoFar);
        }

        nextGeneration(data, param, flags);

        mutate(data.chromosomes, param.mutationProbability, 
            param.size / chromosomesPerBin, param.stateVec);
 

        /* All ones, except for the left-most bitsPerChromosome bits. */
        std::uint64_t mask = (1 << (64 - param.bitsPerChromosome)) - 1;
        data.chromosomes[0] = (data.chromosomes[0] & mask) | data.elites;

        loopCounter += 1;

        if ((flags & 0x000000F0u) == UNKNOWN_CONVERGENCE) {
            keepGoing = (loopCounter - maxIterationSoFar < param.convergence);
        } else {
            keepGoing = (maxSoFar < param.maximum);
        }
    }

    if ((flags & 0x0000000Fu) == RETURN_VALUE) {
        return argmaxSoFar;
    } else {
        return loopCounter;
    }
}

struct Data dataCreator(struct Parameters param)
{
    struct Data data;
    int chromosomesPerBin = 64 / param.bitsPerChromosome; 

    data.chromosomes = new std::uint64_t[param.size / chromosomesPerBin];
    data.survivors = new std::uint64_t[(int) (param.size / chromosomesPerBin / param.survivorFraction)];
    data.fitnessValues = new double[param.size];

    return data;
}

void destroyData(struct Data data)
{
    delete[] data.chromosomes;
    delete[] data.survivors;
    delete[] data.fitnessValues;
}

void checkParameters(struct Parameters& param) {
    /* In case this was not done yet. */
    param.stateVec = seedWithPcg(param.state);

    /* We need size to be a multiple of the number of chromosomes in a 
       bin, so that we do not have a half empty bin. We need the number of bins 
       to be divisible by four for mutation, and the number of  bins not 
       containing survivors to be divisible by eight for the crossover. */

    int chromosomesPerBin = 64 / param.bitsPerChromosome;

    if (param.size % chromosomesPerBin != 0) {
        param.size -= (param.size % param.bitsPerChromosome);
        std::cout << "We had a half-empty bin, so size rounded down to " <<
            param.size << std::endl;
    }

    int numberOfBins = param.size / chromosomesPerBin;
    int survivorBins = numberOfBins / param.survivorFraction;
    int remainder = (numberOfBins - survivorBins) % 8;
    
    if (remainder != 0) {
        /* We want to change survivorFraction into survivorFraction' such that the 
           associated survivorBins' is such that numberOfBins - survivorBins' = 
           numberOfBins - survivorBins - remainder. By simple calculation we get:  */
        param.survivorFraction = (double) numberOfBins / (double) (survivorBins + remainder);
        std::cout << "The number of total bins minus the number of survivor bins was not" <<
            " divisible by eight. Hence we rounded survivorFraction to " << param.survivorFraction
            << std::endl;
    }

    /* Sanity check in case something went wrong with rounding. */
    int newSurvivorBins = numberOfBins / param.survivorFraction;
    if ((numberOfBins - newSurvivorBins) % 8 != 0) {
        std::cout << "Something went wrong in checkParameters." << std::endl;
    }
}
