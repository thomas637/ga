#include "../include/pKnapsack.hpp"

/* Reorders the chromosomes among processors. P(s) sends its kth chunk
   of chromosomes to P(k) and stores it there as the sth chunk. 
   (So an MPI_alltoall on chromosomes.) The variable chromosomes needs 
   to be registered. */
void migrate(std::uint64_t* chromosomes, int totalBins)
{
    /* Elements for all blocks, except the last block, 
       if totalBins does not get neatly divided. */
    int elementsPerBlock = totalBins / bsp_nprocs();
    int elementsOfLastBlock = totalBins - (bsp_nprocs() - 1) * elementsPerBlock;
    int s = bsp_pid();

    for (int k = 0; k < bsp_nprocs(); k++) {
        int startIndex = elementsPerBlock * k;
        
        size_t messageSize = (k == bsp_nprocs() - 1) ?
            elementsPerBlock* sizeof(std::uint64_t) : 
            elementsOfLastBlock * sizeof(std::uint64_t);
        
        bsp_put(k, chromosomes + startIndex, chromosomes, 
            s * elementsPerBlock * sizeof(std::uint64_t), messageSize);
    } 

    bsp_sync();
}

std::uint64_t bspGeneticAlgorithm(struct KnapsackProblem problem, struct Parameters param, 
    std::uint64_t seed, int flags)
{
    /* Give each processor a different seed, produced with an rng different
       from the xorshift we use internally. */
    std::mt19937 rng;
    rng.seed(seed);
    std::uniform_int_distribution<std::uint64_t> uniform;
    
    for (int i = 0; i < bsp_pid(); i++) {
        uniform(rng);
    }

    param.state = uniform(rng);

    /* For storing the optima found on each processor. */
    std::uint64_t* localOptima = new std::uint64_t[bsp_nprocs()];
    bsp_push_reg(localOptima, bsp_nprocs() * sizeof(std::uint64_t));

    /* For migration between processors. */
    struct Data data = dataCreator(param);
    int chromosomesPerBin = 64 / param.bitsPerChromosome;
    int totalBins = param.size / chromosomesPerBin;
    bsp_push_reg(data.chromosomes, totalBins * sizeof(std::uint64_t));
 
    bsp_sync();
    
    /* First run. */
    std::uint64_t localOptimum1 = knapsackGenetic(problem, param, 
        RETURN_ITERATIONS | flags, data);
    
//    std::cout << "Before migration from P(" << bsp_pid() << "): "
//     << toString(data.chromosomes[0]) << std::endl;
    
    migrate(data.chromosomes, totalBins);

//    std::cout << "After migration from P(" << bsp_pid() << "): "
//     << toString(data.chromosomes[0]) << std::endl;

    /* Final run. */
    std::uint64_t localOptimum2 = knapsackGenetic(problem, param,
        RETURN_VALUE | flags, data); 

    double value1 = objectiveValue(localOptimum1, problem);
    double value2 = objectiveValue(localOptimum2, problem);
    std::uint64_t localOptimum = (value1 > value2) ? 
        localOptimum1 : localOptimum2;

    /* Communicate local optima to all processors, and return */ 
    int s = bsp_pid();
    for (int k = 0; k < bsp_nprocs(); k++) {
        bsp_put(k, &localOptimum, localOptima, s * sizeof(std::uint64_t), 
            sizeof(std::uint64_t));
    }

    bsp_sync();

    std::uint64_t globalMax = localOptima[0];
    double value = objectiveValue(globalMax, problem);

    for (int k = 1; k < bsp_nprocs(); k++) {
        double valueK = objectiveValue(localOptima[k], problem);

        if (valueK > value) {
            globalMax = localOptima[k];
            value = valueK;
        }
    }

    bsp_pop_reg(localOptima);
    bsp_pop_reg(data.chromosomes);
    destroyData(data);

    delete[] localOptima;

    return globalMax;
}
