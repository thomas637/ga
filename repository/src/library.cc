#include "../include/library.hpp"

std::uint64_t geneticAlgorithm(
    std::function<double(std::uint64_t chromosome)> objectiveFunction,
    struct Parameters param, int flags)
{
    /* Data allocation and initalization. */
    // ----------------------------------------------------------------------------------------
    checkParameters(param);

    struct Data data = dataCreator(param);

    int chromosomesPerBin = 64 / param.bitsPerChromosome;

    /* Random initialisation. */
    for (int i = 0; i < param.size / chromosomesPerBin; i += 4)
    {
        (param.stateVec).store(data.chromosomes + i);
        xorshift(param.stateVec);
    }

    /* The main loop of the algorithm. */
    // ----------------------------------------------------------------------------------------

    std::uint64_t loopCounter = 0;
    double maxSoFar;        // The maximum objective value in all iterations so far.
    int maxIterationSoFar = 0;    // The iteration this was achieved.
    std::uint64_t argmaxSoFar = 0; // The maximiser.
    bool keepGoing = true;

    while (keepGoing)
    {
        calculateObjectives(data, param, objectiveFunction);

        auto maxIterator = std::max_element(data.fitnessValues, data.fitnessValues + param.size);
        int maxIndex = std::distance(data.fitnessValues, maxIterator);
        double max = *maxIterator;

        if (loopCounter == 0) {
            maxSoFar = max;
            fillBin(param.bitsPerChromosome, data.chromosomes, maxIndex, 0, argmaxSoFar);
        }

        if ((flags & 0x00000F00u) == ELITES)
        {
            std::uint64_t argmax = 0;
            fillBin(param.bitsPerChromosome, data.chromosomes, maxIndex, 0, argmax);
            data.elites[0] = argmax;
        }

        if (max > maxSoFar)
        {
            maxSoFar = max;
            maxIterationSoFar = loopCounter;

            argmaxSoFar = 0;
            fillBin(param.bitsPerChromosome, data.chromosomes, maxIndex, 0, argmaxSoFar);
        }

        nextGeneration(data, param, flags);

        mutate(data.chromosomes, param.mutationProbability,
               param.size / chromosomesPerBin, param.stateVec);

        /* Replace the first chromosome with the elite, which was
           saved before it got mutated or (not) selected. */
        if ((flags & ELITES) == ELITES)
        {
            /* All ones, except for the left-most bitsPerChromosome bits. */
            std::uint64_t mask = (1 << (64 - param.bitsPerChromosome)) - 1;
            data.chromosomes[0] = (data.chromosomes[0] & mask) | data.elites[0];
        }
        

        loopCounter += 1;

        if ((flags & 0x0000F000u) == UNKNOWN_CONVERGENCE)
        {
            keepGoing = (loopCounter - maxIterationSoFar < param.convergence);
        }
        else
        {
            keepGoing = (maxSoFar < param.maximum);
        }
    }

    destroyData(data);

    if ((flags & RETURN_VALUE) == RETURN_VALUE)
    {
        return argmaxSoFar;
    }
    else
    {
        return loopCounter;
    }
}

void calculateObjectives(struct Data data, struct Parameters param,
                         std::function<double(std::uint64_t chromosome)> objectiveFunction)
{
    int chromosomesPerBin = 64 / param.bitsPerChromosome;

    for (int i = 0; i < param.size; i += chromosomesPerBin)
    {
        /* We have to evaluate this left to right. */
        std::uint64_t chromosome = data.chromosomes[i / chromosomesPerBin];
        for (int j = 0; j < chromosomesPerBin; j++)
        {
            data.fitnessValues[i + j] = objectiveFunction(chromosome);
            chromosome <<= param.bitsPerChromosome;
        }
    }
}

void calculateScaledObjectives(struct Data data, struct Parameters param,
                         std::function<double(std::uint64_t chromosome)> objectiveFunction)
{
    calculateObjectives(data, param, objectiveFunction);
    
    auto maxIterator = std::max_element(data.fitnessValues, data.fitnessValues + param.size);
    auto minIterator = std::min_element(data.fitnessValues, data.fitnessValues + param.size);

    if (*minIterator < *maxIterator) {
        for (int i = 0; i < param.size; i++) {
            data.fitnessValues[i] = (data.fitnessValues[i] - *minIterator) / (*maxIterator - *minIterator);
        }
    }
}

/* Generates survivors (with reselection) by taking 'tournamentSize' random individuals,
   and passing along the fittest. We assume numberOfSurvivors is a multiple of the number
   of chromosomes fitting into a bin. With reselection */
void selectTournamentReselection(struct Data data, int numberOfSurvivors,
                                 int tournamentSize, struct Parameters &param)
{
    int chromosomesPerBin = 64 / param.bitsPerChromosome;

    /* In each of these iterations, we fill one bin. */
    for (int i = 0; i < numberOfSurvivors; i += chromosomesPerBin)
    {

        /* We fill this bin this loop, start empty, so all zeroes. */
        std::uint64_t bin = 0;
        for (int j = 0; j < chromosomesPerBin; j++)
        {

            /* Start of tournament for the jth spot. */
            int randomIndex = xorshiftDouble(param.state) * param.size;

            double max = data.fitnessValues[randomIndex];
            double argmax = randomIndex;

            /* Check whether the next participant beats the previous one. */
            for (int j = 1; j < tournamentSize; j++)
            {
                randomIndex = xorshiftDouble(param.state) * param.size;

                if (data.fitnessValues[randomIndex] > max)
                {
                    max = data.fitnessValues[randomIndex];
                    argmax = randomIndex;
                }
            }

            /* End of the tournament for the jth spot, won by the chromosome
               in position argmax. */
            fillBin(param.bitsPerChromosome, data.chromosomes, argmax,
                    j, bin);
        }

        /* i is the chromosome index, so we divide to get the bin index. */
        data.survivors[i / chromosomesPerBin] = bin;
    }
}

/* Generates survivors (with reselection) by taking 'tournamentSize' random individuals,
   and passing along the fittest. We assume numberOfSurvivors is a multiple of the number
   of chromosomes fitting into a bin. Without reselection.*/
void selectTournamentNoReselection(struct Data data, int numberOfSurvivors,
                                 int tournamentSize, struct Parameters &param)
{
    int chromosomesPerBin = 64 / param.bitsPerChromosome;
    int numberOfCandidates = param.size; // counter of how many candidate survivors there still are
    int candidates[param.size];          // int array that keeps track which chromosomes have already been selected
    int argmax = 0;

    /* In each of these iterations, we fill one bin. */
    for (int i = 0; i < numberOfSurvivors; i += chromosomesPerBin)
    {

        /* We fill this bin in this loop, so we start empty (all zeroes). */
        std::uint64_t bin = 0;
        for (int j = 0; j < chromosomesPerBin; j++)
        {

            /* Start of tournament for the jth spot. */
            int randomIndex = xorshiftDouble(param.state) * numberOfCandidates;

            /* find the first randomIndex^th zero-value in candidates: */
            int searchIndex = 0;
            int counterOfZeroes = 0;

            while (searchIndex < param.size)
            {
                if (candidates[searchIndex] == 0)
                {
                    // we have encountered a candidate chromosome!
                    counterOfZeroes += 1;
                }
                if (counterOfZeroes == randomIndex)
                {
                    // found the selected chromosome!
                    break;
                }
            }
            double max = data.fitnessValues[searchIndex];
            argmax = searchIndex;

            /* Find the next chromosomes in this tournament. */
            for (int j = 1; j < tournamentSize; j++)
            {
                randomIndex = xorshiftDouble(param.state) * numberOfCandidates;

                /* find the first randomIndex^th zero-value in candidates: */
                int searchIndex = 0;
                int counterOfZeroes = 0;

                while (searchIndex < param.size)
                {
                    if (candidates[searchIndex] == 0)
                    {
                        // we have encountered a candidate chromosome!
                        counterOfZeroes += 1;
                    }
                    if (counterOfZeroes == randomIndex)
                    {
                        // found the selected chromosome!
                        break;
                    }
                }

                /* check whether this chromosome beats the current best */
                if (data.fitnessValues[searchIndex] > max)
                {
                    max = data.fitnessValues[searchIndex];
                    argmax = searchIndex;
                }
            }

            /* end of tournament, one chromosome has been selected and so: */
            numberOfCandidates -= 1;
            candidates[searchIndex] = 1;

            /* End of the tournament for the jth spot, won by the chromosome
               in position argmax. */
            fillBin(param.bitsPerChromosome, data.chromosomes, argmax,
                    j, bin);
        }

        /* i is the chromosome index, so we divide to get the bin index. */
        data.survivors[i / chromosomesPerBin] = bin;
    }
}

/* Generates survivors (with reselection) by taking 'numberOfSurvivors' individuals, with
 * probability proportional to its fitness. We assume numberOfSurvivors is a multiple of
 * the number of chromosomes fitting into a bin. */
void selectProportionalReselection(struct Data data, struct Parameters &param,
                                   int numberOfSurvivors)
{

    // get the fitness values of the chromosomes and calculate sum
    double *fitnessValues = data.fitnessValues;
    double totalFitness = 0;
    for (int i = 0; i < param.size; ++i)
        totalFitness += fitnessValues[i];

    int chromosomesPerBin = 64 / param.bitsPerChromosome;

    // realize numberOfSurvivors Survivors
    for (int i = 0; i < numberOfSurvivors; i += chromosomesPerBin)
    {

        std::uint64_t bin = 0;

        for (int j = 0; j < chromosomesPerBin; ++j)
        {
            // get random number uniformly in [0, totalFitness]
            double u = xorshiftDouble(param.state) * totalFitness;

            // each chromosome has an interval of size proportional to its fitness
            // determine in which interval u falls
            // select this chromosome as survivor
            int index = 0;
            double accumulatedFitness = 0;
            while (index < numberOfSurvivors)
            {
                double rightAccFitness = accumulatedFitness + data.fitnessValues[index];
                if (accumulatedFitness < u and u < rightAccFitness) // found chromosome
                {
                    break;
                }
                //continue search
                accumulatedFitness = rightAccFitness;
                index += 1;
            }

            fillBin(param.bitsPerChromosome, data.chromosomes, index,
                    j, bin);
        }

        data.survivors[i / chromosomesPerBin] = bin;
    }
}

/* Generates survivors (with reselection) by taking 'numberOfSurvivors' individuals, with
 * probability proportional to its fitness. We assume numberOfSurvivors is a multiple of
 * the number of chromosomes fitting into a bin. Without reselection. */
void selectProportionalNoReselection(struct Data data, struct Parameters &param,
                                     int numberOfSurvivors)
{
    /* Calculate total fitness in population */
    double totalFitness = 0;
    for (int i = 0; i < param.size; i++)
    {
        totalFitness += data.fitnessValues[i];
    }

    int chromosomesPerBin = 64 / param.bitsPerChromosome;

    // realize numberOfSurvivors Survivors
    for (int i = 0; i < numberOfSurvivors; i += chromosomesPerBin)
    {
        std::uint64_t bin = 0;

        for (int j = 0; j < chromosomesPerBin; ++j)
        {
            // get random number uniformly in [0, totalFitness]
            double u = xorshiftDouble(param.state) * totalFitness;

            // each chromosome has an interval of size proportional to its fitness
            // determine in which interval u falls
            // select this chromosome as survivor
            int index = 0;
            double accumulatedFitness = 0;
            while (index < numberOfSurvivors)
            {
                double rightAccFitness = accumulatedFitness + data.fitnessValues[index];
                if (accumulatedFitness < u and u < rightAccFitness) // found chromosome
                {
                    break;
                }
                //continue search
                accumulatedFitness = rightAccFitness;
                index += 1;
            }

            fillBin(param.bitsPerChromosome, data.chromosomes, index,
                    j, bin);

            /* set fitnessvalue of selected chromosome to 0 to prevent reselection */
            totalFitness -= data.fitnessValues[index];
            data.fitnessValues[index] = 0;
        }

        data.survivors[i / chromosomesPerBin] = bin;
    }
}

/* Overwrites mothers1 and mothers2 with their uniformly crossover'd children.  */
inline void crossover(Vec4uq &mother1, Vec4uq &mother2, Vec4uq &state)
{
    Vec4uq mask = xorshift(state);

    Vec4uq child1;
    child1 = (mother1 & mask) | (mother2 & ~mask);

    mother2 = (mother1 & ~mask) | (mother2 & mask);

    mother1 = child1;
}

/* Flips the bits in an array bitstrings of 'length' bins with
   probability 2^-n. Assumes 4 divides length. state is the state
   of the Xorshift which is used to introduce the randomness. */
void mutate(std::uint64_t *bitstrings, int n, int length, Vec4uq &state)
{
    Vec4uq packedInts;

    for (int i = 0; i < length; i += 4)
    {
        packedInts.load(bitstrings + i); // The bits we flip this iteration.

        /* x and y = 1 iff x and y are one, so P(x and y = 1) 
           is equal to P(x = 1) P(x = y) if x and y are independent. 
           So when we bitwise-and n uniform random integers, the bits
           are one with probability 2^-n. */
        Vec4uq bitMask = xorshift(state);
        for (int j = 1; j < n; j++)
        {
            bitMask &= xorshift(state);
        }

        /* 0 xor 0 = 0, 1 xor 0 = 1, 0 xor 1 = 1, 1 xor 1 = 0, so
           xoring a bit with 1 flips it, xoring with 0 does nothing. */
        packedInts ^= bitMask;
        packedInts.store(bitstrings + i);
    }
}

/* Uniform random crossover of all chromosomes in
   parents[i] with the chromosomes in parents[i + 4]. 
   Assumes numberOfParentBins is a multiple of 8. */
void crossoverInPlace(std::uint64_t *parents, int numberOfParentBins, Vec4uq &state)
{
    Vec4uq mothers1, mothers2;
    for (int i = 0; i < numberOfParentBins; i += 8)
    {
        mothers1.load(parents + i);
        mothers2.load(parents + i + 4);

        crossover(mothers1, mothers2, state);

        mothers1.store(parents + i);
        mothers2.store(parents + i + 4);
    }
}

void nextGeneration(struct Data data, struct Parameters param, int flags)
{
    int chromosomesPerBin = 64 / param.bitsPerChromosome;

    /* Round so it fits neatly into bins. */
    // param.size and chromosomesPerBin are integers, so the first 
    // fraction is truncated. This is correct, as we have paddings in the bin.
    // It is then cast to double for division by param.survivorFraction, the 
    // result rounded down back to an int. 
    int survivorBins = param.size / chromosomesPerBin / param.survivorFraction;
    int generationBins = param.size / chromosomesPerBin;

    if ((flags & 0x000000F0u) == TOURNAMENT)
    {
        selectTournamentReselection(data, survivorBins * chromosomesPerBin, param.tournamentSize, param);
    }
    else if((flags & 0x000000F0u) == TOURNAMENT_NORESELECT)
    {
        selectTournamentNoReselection(data, survivorBins * chromosomesPerBin, param.tournamentSize, param);
    }
    else if((flags & 0x000000F0u) == PROPORTIONAL)
    {
        selectProportionalReselection(data, param, survivorBins * chromosomesPerBin);
    }
    else
    {
        selectProportionalNoReselection(data, param, survivorBins * chromosomesPerBin);
    }

    /* Replace first survivor by the elite. */
    if ((flags & 0x00000200u) == ELITES)
    {
        /* zero out first chromosome. */
        std::uint64_t mask = (1 << (64 - param.bitsPerChromosome)) - 1;
        data.survivors[0] &= mask;

        /* Fill with elite. */
        data.survivors[0] |= data.elites[0];
    }

    /* The survivors make up the first part of the new generation. */
    memcpy(data.survivors, data.chromosomes,
           survivorBins * sizeof(std::uint64_t));

    /* As the order the survivors are selected in, is arbitrary, taking the first 1/nth part,
       means we select survivors with probability 1/n to be selected for reproduction. */

    /* Fill the remaining space of 'chromosomes' with randomly selected couples. */
    for (int i = survivorBins; i < generationBins; i++)
    {
        data.chromosomes[i] = randomBin(param.bitsPerChromosome, data.survivors, param.state,
            survivorBins * chromosomesPerBin / param.parentFraction);
    }

    crossoverInPlace(data.chromosomes + survivorBins, generationBins - survivorBins, param.stateVec);
}

struct Data dataCreator(struct Parameters param)
{
    struct Data data;
    int chromosomesPerBin = 64 / param.bitsPerChromosome;

    data.chromosomes = new std::uint64_t[param.size / chromosomesPerBin];
    data.survivors = new std::uint64_t[(int) (param.size / chromosomesPerBin / param.survivorFraction)];
    data.elites = new std::uint64_t[1];
    data.elites[0] = 0;
    data.fitnessValues = new double[param.size];

    return data;
}

void destroyData(struct Data data)
{
    delete[] data.chromosomes;
    delete[] data.survivors;
    delete[] data.elites;
    delete[] data.fitnessValues;
}

void checkParameters(struct Parameters& param) {

    param.stateVec = seedWithPcg(param.state);

    /* We need size to be a multiple of the number of chromosomes in a 
       bin, so that we do not have a half empty bin. We need the number of bins 
       to be divisible by four for mutation, and the number of  bins not 
       containing survivors to be divisible by eight for the crossover. */

    int chromosomesPerBin = 64 / param.bitsPerChromosome;

    if (param.size % chromosomesPerBin != 0) {
        param.size -= (param.size % param.bitsPerChromosome);
        std::cout << "We had a half-empty bin, so size rounded down to " <<
            param.size << std::endl;
    }

    int binsOverMultipleOfFour = (param.size / chromosomesPerBin) % 4;
    if (binsOverMultipleOfFour != 0) {
        
        /* We need param.size' / chromosomesPerBin = param.size / chromosomesPerBin -
           binsOverMultipleOfFour, so param.size' = chromosomesPerBin(param.size / chromosomesPerBin
           - binsOverMultipleOfFour). */
        param.size = chromosomesPerBin * (param.size / chromosomesPerBin - binsOverMultipleOfFour);
        std::cout << "The number of bins is not divisible by four, so size rounded to " <<
            param.size << std::endl;
    }

    int numberOfBins = param.size / chromosomesPerBin;
    int survivorBins = numberOfBins / param.survivorFraction;
    int remainder = (numberOfBins - survivorBins) % 8;
    
    if (remainder != 0) {
        /* We want to change survivorFraction into survivorFraction' such that the 
           associated survivorBins' is such that numberOfBins - survivorBins' = 
           numberOfBins - survivorBins - remainder. By simple calculation we get:  */
        param.survivorFraction = (double) numberOfBins / (double) (survivorBins + remainder);
        std::cout << "The number of total bins minus the number of survivor bins was not" <<
            " divisible by eight. Hence we rounded survivorFraction to " << param.survivorFraction
            << std::endl;
    }

    /* Sanity check in case something went wrong with rounding. */
    int newSurvivorBins = numberOfBins / param.survivorFraction;
    if ((numberOfBins - newSurvivorBins) % 8 != 0) {
        std::cout << "Something went wrong in checkParameters." << std::endl;
    }
}

void printChromosomes(struct Data generation, struct Parameters parameters)
{
    std::cout << "The chromosomes in this generation are: " << std::endl;

    unsigned int numberOfChromosomes = parameters.size;
    unsigned int bitsPerChromosome = parameters.bitsPerChromosome;
    unsigned int chromosomesPerBin = std::min(numberOfChromosomes, 64 / bitsPerChromosome);                               // floor function
    unsigned int numberOfBins = numberOfChromosomes / chromosomesPerBin + (numberOfChromosomes % chromosomesPerBin != 0); // ceiling function

    std::uint64_t *integers;
    integers = generation.chromosomes;
    std::uint64_t integer = 0;

    // loop over all the bins (int64)
    for (unsigned int i = 0; i < numberOfBins; ++i)
    {
        integer = integers[i];

        // loop over all chromosomes in this bin
        for (unsigned int j = 0; j < chromosomesPerBin; ++j)
        {

            std::cout << "[";

            for (unsigned int i = 0; i < bitsPerChromosome; ++i)
            {
                if ((integer & 0x8000000000000000u) == 0)
                {
                    std::cout << "0";
                }
                else
                {
                    std::cout << "1";
                }
                integer <<= 1; // throw away left-most bit in integer
            }

            std::cout << "]" << std::endl;
        }
    }
}
