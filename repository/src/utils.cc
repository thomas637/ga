#include <iostream>
#include <bitset>
#include "../include/utils.hpp"

void printBits(std::uint64_t bitstring){
    std::cout << std::bitset<64>(bitstring) << std::endl;
}

int countOnes(std::uint64_t integer)
{
    int counter = 0;

    for (int i = 0; i < 64; i++) {
        counter += (integer & 1);
        integer >>= 1;    
    }
    
    return counter;
}

std::string toString(std::uint64_t integer){
    std::string bitString = "";
    for (unsigned int i = 0; i < 64; ++i){
        if ((integer & 0x8000000000000000u) == 0){
            bitString.append(std::to_string(0));
        }
        else{
            bitString.append(std::to_string(1));
        }
        integer <<= 1; // throw away left-most bit in integer
    }
    return bitString;
}

std::uint64_t binaryToGrayCode(std::uint64_t bitString){
    // get left-most bit
    // copy this value into newBitString
    std::uint64_t newBitString;
    if ((bitString & 0b1000000000000000000000000000000000000000000000000000000000000000u) > 0)
    {
        newBitString = 0b1000000000000000000000000000000000000000000000000000000000000000u;
    }
    else
    {
        newBitString = 0b0000000000000000000000000000000000000000000000000000000000000000u;
    }

    for (int i = 62; i >= 0; --i)
    {
        // create a mask that inspects two neigbouring bits with right most bit at index i
        std::uint64_t bitstr = bitString;
        std::uint64_t mask = 0b0000000000000000000000000000000000000000000000000000000000000011u;
        mask = (mask << i);

        // check whether both i and i+1 are the same
        if (!(((bitstr & mask) >> i) > 2 || ((~bitstr & mask) >> i) > 2))
        {
            // set bit i to 1
            std::uint64_t mask_2 =  0b0000000000000000000000000000000000000000000000000000000000000001u;
            mask_2 = (mask_2 << i);
            newBitString = (newBitString | mask_2);
        }
    }
    return newBitString;
}

/* input is a 64-bitstring with the 32 left-most bits 0 */
std::uint64_t binaryToGrayCode32(std::uint64_t bitString){

    // get left-most bit
    // copy this value into newBitString
    std::uint64_t newBitString;
    if ((bitString & 0b1000000000000000000000000000000000000000000000000000000000000000u) > 0)
    {
        newBitString = 0b1000000000000000000000000000000000000000000000000000000000000000u;
    }
    else
    {
        newBitString = 0b0000000000000000000000000000000000000000000000000000000000000000u;
    }

    for (int i = 62; i >= 0; --i)
    {
        // create a mask that inspects two neigbouring bits with right most bit at index i
        std::uint64_t bitstr = bitString;
        std::uint64_t mask = 0b0000000000000000000000000000000000000000000000000000000000000011u;
        mask = (mask << i);

        // check whether both i and i+1 are the same
        if (!(((bitstr & mask) >> i) > 2 || ((~bitstr & mask) >> i) > 2))
        {
            // set bit i to 1
            std::uint64_t mask_2 =  0b0000000000000000000000000000000000000000000000000000000000000001u;
            mask_2 = (mask_2 << i);
            newBitString = (newBitString | mask_2);
        }
    }
    return newBitString;
}

std::uint64_t grayCodeToBinary(std::uint64_t grayCode){
    // get left-most bit
    // copy this value into binary
    std::uint64_t binary;
    if ((grayCode & 0b1000000000000000000000000000000000000000000000000000000000000000u) > 0)
    {
        binary = 0b1000000000000000000000000000000000000000000000000000000000000000u;
    }
    else
    {
        binary = 0b0000000000000000000000000000000000000000000000000000000000000000u;
    }

    for (int i = 62; i >= 0; --i)
    {
        // create masks that inspect bits binary i + 1 and grayCode i
        std::uint64_t copyGray = grayCode;
        std::uint64_t copyBinary = binary;

        std::uint64_t mask = 0b0000000000000000000000000000000000000000000000000000000000000001u;
        mask = (mask << i);

        if ((copyGray & mask) > 0){
            // grayCode has a one at bit i
            mask = (mask << 1);
            if ((copyBinary & mask) == 0)
            {
                // binary has a zero at bit i + 1
                // set bit i of b to 1
                mask = (mask >> 1);
                binary = (binary | mask);
            }

        }
        else{
            // grayCode has a zero at bit i

            mask = (mask << 1);
            if ((copyBinary & mask) > 0)
            {
                // binary has a one at bit i + 1
                // set bit i of b to 1
                mask = (mask >> 1);
                binary = (binary | mask);
            }
        }
    }
    return binary;
}

std::uint32_t grayCodeToBinary(std::uint32_t grayCode){
    // get left-most bit
    // copy this value into binary
    std::uint32_t binary;
    if ((grayCode & 0x80000000u) > 0)
    {
        binary = 0x80000000u;
    }
    else
    {
        binary = 0;
    }

    for (int i = 30; i >= 0; --i)
    {
        // create masks that inspect bits binary i + 1 and grayCode i
        std::uint32_t copyGray = grayCode;
        std::uint32_t copyBinary = binary;

        std::uint32_t mask = 1;
        mask = (mask << i);

        if ((copyGray & mask) > 0){
            // grayCode has a one at bit i
            mask = (mask << 1);
            if ((copyBinary & mask) == 0)
            {
                // binary has a zero at bit i + 1
                // set bit i of b to 1
                mask = (mask >> 1);
                binary = (binary | mask);
            }

        }
        else{
            // grayCode has a zero at bit i

            mask = (mask << 1);
            if ((copyBinary & mask) > 0)
            {
                // binary has a one at bit i + 1
                // set bit i of b to 1
                mask = (mask >> 1);
                binary = (binary | mask);
            }
        }
    }
    return binary;
}
