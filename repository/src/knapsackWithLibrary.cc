#include "../include/knapsackWithLibrary.hpp"

double objectiveValue(std::uint64_t item, 
    struct KnapsackProblem& problem)
{
    double price = 0;
    double weight = 0;

    for (int i = 0; i < problem.numberOfItems; i++) {
        if ((item & 0x8000000000000000u) > 0) {
            price += problem.prices[i];
            weight += problem.weights[i];
        }
        item <<= 1;
    }

    if (weight <= problem.weightLimit) {
        return price;
    } else {
        return 0;
    }
}

std::uint64_t knapsackGenetic(struct KnapsackProblem problem, 
    struct Parameters param, int flags)
{
    param.bitsPerChromosome = problem.numberOfItems;
    
    return geneticAlgorithm(
        [&problem](std::uint64_t item) {
            return objectiveValue(item, problem);
        },
        param, flags);
}
