#include "../include/utils.hpp"
#include <iostream>

bool testBinIndexAndSpace()
{
    bool passed = true;

    /* 4 chromosomes per bin
       0 1 2 3 | 4 5 6 7 | 8 9 10 11 | 12 13 14 15 
       so we should be in the third bin (starting the count 
       from zero, and have 2 * 16 = 32 bits to the left. */
    int bitsPerChromosome = 16;
    int index = 14;
    passed = passed && (binIndex(index, bitsPerChromosome) == 3);
    passed = passed && (leftSpace(index, bitsPerChromosome) == 32);

    /* Insert test where the bins are not filled up all the way. */

    return passed;
}

/* Generates a population with 20% 1s, 80%s zeros. With random selection,
   the bins should have 10% 1s as well. maxError is how large 
   [expected ones] / [observed ones] and [observed ones] / [expected ones]
   may be to still pass the test. */
bool testRandomBin(double maxError)
{
    int popSize = 100000;
    std::uint64_t* bins = new std::uint64_t[popSize];

    for (int i = 0; i < 100000; i+= 5) {
        bins[i] = 0xFFFFFFFFFFFFFFFFu;
        bins[i + 1] = 0;
        bins[i + 2] = 0;
        bins[i + 3] = 0;
        bins[i + 4] = 0;
    } 

    double totalOnes = 0;
    std::uint64_t state = 93240958320495;

    int randomSize = 10000;

    for (int i = 0; i < randomSize; i++) {
        totalOnes += countOnes(randomBin(16, bins, state, popSize));
    }

    double expectedOnes = randomSize * 64 / 5;

    double largestFraction = (totalOnes > expectedOnes) ? 
        totalOnes / expectedOnes : expectedOnes / totalOnes;

    delete[] bins;

    return (largestFraction < maxError);
}

bool testFillBin()
{
    int bitsPerChromosome = 32;
    int numberOfBins = 1;
    std::uint64_t state = 34503405;
    std::uint64_t* population = new std::uint64_t[numberOfBins]; // 2000 uninitialised chromosomes. 

    for (int i = 0; i < numberOfBins; i++) {
        population[i] = xorshift(state);
    }

    /* We copy population to here using fillBin. */
    std::uint64_t* populationCopy = new std::uint64_t[1000];
    std::uint64_t bin = 0;
    for (int i = 0; i < numberOfBins * 2; i += 2) {
        bin = 0;
        fillBin(bitsPerChromosome, population, i, 0, bin);
        fillBin(bitsPerChromosome, population, i + 1, 1, bin);
        populationCopy[i] = bin;
    }    

    bool equal = true;

    for (int i = 0; i < numberOfBins; i++) {
        if (population[i] != populationCopy[i]) {
            equal = false;
        }
    }

    delete[] population;
    delete[] populationCopy;

    return equal;
}

void testToString(std::uint64_t integer){
    std::cout << "The bit you entered is: " << std::endl;
    std::cout << toString(integer) << std::endl;
}

int main()
{
    if (testBinIndexAndSpace()) {
        std::cout << "Bin index and space passed." << std::endl;
    } else {
        std::cout << "Bin index and space failed." << std::endl;
    }

    if (testRandomBin(1.01)) {
        std::cout << "Random bin passed at 1.01" << std::endl;
    } else {
        std::cout << "Random bin failed at 1.01" << std::endl;
    }

    if (testFillBin()) {
        std::cout << "Fill bin passed." << std::endl;
    } else {
        std::cout << "Fill bin failed." << std::endl;
    }
    testToString(0b1010101010101010101010101010101010101010101010101010101010101010u);
}
