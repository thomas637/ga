#include "../include/pKnapsack.hpp"
#include "include/problemGenerator.hpp"
#include <iostream>

void test()
{
    bsp_begin(bsp_nprocs());
    struct Parameters param;
    param.mutationProbability = 4;
    param.convergence = 100;
    param.size = 4096; // with the larger population size of 32768, it actually does not find the maximum.
    param.survivorFraction = 64;
    param.parentFraction = 1;
    param.bitsPerChromosome = 32;
    param.maximum = param.bitsPerChromosome - 1;

    struct KnapsackProblem problem = smallProblem(param.bitsPerChromosome);
 
    std::uint64_t knapsack = bspGeneticAlgorithm(problem, 
        param, 29349934, UNKNOWN_CONVERGENCE);

//    printf("Maximum found by P(%d) is %lf. Actual maximum is 13549094\n",
//        bsp_pid(), objectiveValue(knapsack, 24, weights, prices, weightLimit));

    if (bsp_pid() == 0) {
        std::cout << bsp_nprocs() << "," <<
         (int) objectiveValue(knapsack, problem);
    }

    bsp_end();
}

int main(int argc, char **argv)
{
    bsp_init(test, argc, argv);

    test();

    exit(EXIT_SUCCESS);

    return 0;
}
