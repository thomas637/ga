#include "../include/knapsackWithLibrary.hpp"
#include "include/problemGenerator.hpp"
#include <chrono>
#include <fstream>
#include <iostream>

void testSmall()
{
    struct Parameters param;
    param.mutationProbability = 4;
    param.convergence = 1000;
    param.size = 4096; 
    param.survivorFraction = 2;
    param.parentFraction = 1;
    param.bitsPerChromosome = 20;
    param.tournamentSize = 2;
    param.state = 2904390324;
    param.stateVec = seedWithPcg(293920344);

    struct KnapsackProblem problem = smallProblem(param.bitsPerChromosome);
   
    std::uint64_t argmax = knapsackGenetic(problem, param, 
        TOURNAMENT | ELITES | RETURN_VALUE | UNKNOWN_CONVERGENCE);

    std::uint64_t iterations = knapsackGenetic(problem, param, 
        TOURNAMENT | ELITES | RETURN_ITERATIONS | UNKNOWN_CONVERGENCE);

    std::cout << "Found maximum objective value is " << 
        (int) objectiveValue(argmax, problem) << 
        " achieved by " << toString(argmax) <<
        ". Actual maximum is " << param.bitsPerChromosome - 1 << ". This took "
         << iterations << " iterations." << std::endl;

    destroyProblem(problem);
}


void testUnknown()
{
    struct Parameters param;
    param.mutationProbability = 4;
    param.convergence = 1000;
    param.size = 4096;
    param.survivorFraction = 2;
    param.parentFraction = 1;
    param.bitsPerChromosome = 20;
    param.state = 2904390324;
    param.stateVec = seedWithPcg(293920344);
    param.tournamentSize = 2;

    struct KnapsackProblem problem = internetProblem();
   
    std::uint64_t argmax = knapsackGenetic(problem, param, 
        TOURNAMENT | ELITES | RETURN_VALUE | UNKNOWN_CONVERGENCE);

    std::uint64_t iterations = knapsackGenetic(problem, param, 
        TOURNAMENT | ELITES | RETURN_ITERATIONS | UNKNOWN_CONVERGENCE);

    std::cout << "Found maximum objective value is " << 
        (int) objectiveValue(argmax, problem) << 
        " achieved by " << toString(argmax) <<
        ". Actual maximum is 13549094. This took " << iterations <<
        " iterations." << std::endl;

    destroyProblem(problem);
}


void testKnown()
{
    struct Parameters param;
    param.mutationProbability = 4;
    param.size = 4096; // with the larger population size of 32768, it actually does not find the maximum.
    param.survivorFraction = 2;
    param.parentFraction = 1;
    param.bitsPerChromosome = 24;
    param.state = 2904390324;
    param.stateVec = seedWithPcg(293920344);
    param.maximum = 13549094;
    param.tournamentSize = 2;

    struct KnapsackProblem problem = internetProblem();
   
    std::uint64_t iterations = knapsackGenetic(problem, param, 
        TOURNAMENT | ELITES | RETURN_ITERATIONS | KNOWN_CONVERGENCE);

    std::cout << "Convergence to the true maximum took " << iterations << " iterations." << std::endl;

    destroyProblem(problem);
}

void testKnownSmall()
{
    struct Parameters param;
    param.mutationProbability = 4;
    param.size = 4096; // with the larger population size of 32768, it actually does not find the maximum.
    param.survivorFraction = 2;
    param.parentFraction = 1;
    param.bitsPerChromosome = 16;
    param.state = 2904390324;
    param.stateVec = seedWithPcg(293920344);
    param.maximum = param.bitsPerChromosome - 1;
    param.tournamentSize = 2;

    struct KnapsackProblem problem = smallProblem(param.bitsPerChromosome);
   
    std::uint64_t iterations = knapsackGenetic(problem, param, 
        TOURNAMENT | ELITES | RETURN_ITERATIONS | KNOWN_CONVERGENCE);

    std::cout << "Convergence to the true maximum took " << iterations << " iterations." << std::endl;

    destroyProblem(problem);
}

int main()
{
    testUnknown();
    testKnown();
    testSmall();
//    testKnownSmall();
}
