#include "../include/library.hpp"
#include <iostream>

/* Generates survivors (with reselection) by taking 'numberOfSurvivors' individuals, with
 * probability proportional to its fitness. We assume numberOfSurvivors is a multiple of
 * the number of chromosomes fitting into a bin. The indices are stored in indicesOfSurvivors. */
void calculateFitness(struct Data data)
{
    data.fitnessValues[0] = 3.0;
    (data.fitnessValues)[1] = 2.0;
    (data.fitnessValues)[2] = 2.0;
    (data.fitnessValues)[3] = 1.0;
    (data.fitnessValues)[4] = 1.0;
    (data.fitnessValues)[5] = 0.0;
}

void selectProportionalNoReplacement(struct Data data, struct Parameters& param,
                               int numberOfSurvivors, int* indicesOfSurvivors)
{
    // get the fitness values of the chromosomes and calculate sum
    // double * fitnessValuesCopy = new double[param.size];
    double totalFitness = 0;

    for (int i = 0; i < param.size; i++)
    {
        totalFitness += data.fitnessValues[i];
    }

    // realize numberOfSurvivors Survivors
    for (int j = 0; j < numberOfSurvivors; j++)
    {
        // get random number uniformly in [0, totalFitness]
        double u = xorshiftDouble(param.state) * totalFitness;

        // each chromosome has an interval of size proportional to its fitness
        // determine in which interval u falls
        // select this chromosome as survivor
        int index = 0;
        double accumulatedFitness = 0;
        while (index < param.size) {
            double rightAccFitness = accumulatedFitness + data.fitnessValues[index];
            if (accumulatedFitness < u and u < rightAccFitness) // found chromosome
            {
                break;
            }
            accumulatedFitness = rightAccFitness;
            index += 1;
        }

        indicesOfSurvivors[j] = index;

        /* update totalFitness */
        totalFitness -= data.fitnessValues[index];

        /* set fitnessvalue of selected chromosome to 0 to prevent reselection */
        data.fitnessValues[index] = 0;
    }

    std::cout << "____________________________" << std::endl;
}


int main()
{
    std::cout << "Initializing test population" << std::endl;
    std::cout << " " << std::endl;

    // create test chromosomes
    int numberOfSurvivors = 3;

    Data data;
    data.chromosomes = new std::uint64_t[1];
    data.fitnessValues = new double[6];

    double* counterSelectedChromosomes = new double[6];
    int* indicesOfSurvivors = new int[numberOfSurvivors];

    for (int i = 0; i < 6; i++) {
        counterSelectedChromosomes[i] = 0;
    }

    std::uint64_t state = 34959034590345;

    // create mini pool of 6 non-random chromosomes of size 3
    // let fitness function f be f(x) = sum of ones

//    chrom_1 = [1, 1, 1]; // int = 7 // fitness: 3
//    chrom_2 = [1, 1, 0]; // int = 6 // fitness: 2
//    chrom_3 = [0, 1, 1]; // int = 3 // fitness: 2
//    chrom_4 = [1, 0, 0]; // int = 4 // fitness: 1
//    chrom_5 = [0, 0, 1]; // int = 1 // fitness: 1
//    chrom_6 = [0, 0, 0]; // int = 0 // fitness: 0

    (data.fitnessValues)[0] = 3.0;
    (data.fitnessValues)[1] = 2.0;
    (data.fitnessValues)[2] = 2.0;
    (data.fitnessValues)[3] = 1.0;
    (data.fitnessValues)[4] = 1.0;
    (data.fitnessValues)[5] = 0.0;

    // int64_t of this data looks like: 1111100111000010000000000000000000000000000000000000000000000000
    // corresponding int is 17996947060925923328
    (data.chromosomes)[0] = 17996947060925923328u;

    Parameters param;
    param.bitsPerChromosome = 3;
    param.size = 6;
    param.survivorFraction = 2;
    param.state = state;

    // select indices by proportional selection without replacement
    int survivor;
    int T = 1000;

    for (int t = 0; t < T; t++)
    {
        selectProportionalNoReplacement(data, param, 3, indicesOfSurvivors);

        for (int i = 0; i < numberOfSurvivors; i++)
        {
            survivor = indicesOfSurvivors[i];
            std::cout << "selected: chromosome " << survivor + 1 << std::endl;
            counterSelectedChromosomes[survivor] += 1;
        }

        calculateFitness(data);
    }


    std::cout << "Chromosome 1: selected " << counterSelectedChromosomes[0]/T * 100 << "% of the time, ";
    std::cout << "expected 73.492%" << std::endl;

    std::cout << "Chromosome 2: selected " << counterSelectedChromosomes[1]/T * 100 << "% of the time, ";
    std::cout << "expected " << std::endl;

    std::cout << "Chromosome 3: selected " << counterSelectedChromosomes[2]/T * 100 << "% of the time, ";
    std::cout << "expected " << std::endl;

    std::cout << "Chromosome 4: selected " << counterSelectedChromosomes[3]/T * 100 << "% of the time, ";
    std::cout << "expected " << std::endl;

    std::cout << "Chromosome 5: selected " << counterSelectedChromosomes[4]/T * 100 << "% of the time, ";
    std::cout << "expected " << std::endl;

    std::cout << "Chromosome 6: selected " << counterSelectedChromosomes[5]/T * 100 << "% of the time, ";
    std::cout << "expected 0.0%" << std::endl;

    std::cout << "Finished testing proportional Selection" << std::endl;
    std::cout << " " << std::endl;

    // Nog een message aan Thomas:
    // er worden steeds precies dezelfde 3 chromosomen geselecteerd en ik denk dat dat komt doordat
    // param.state niet verandert
    // weet jij daar een oplossing voor?

    // Antwoord aan Fleur: geef param door als reference, dus struct Parameters& param ipv 
    // struct Parameters param. In het laatste geval wordt er een kopie van param gemaakt en 
    // meegegeven aan de functie. Dat kopie wordt aangepast, maar het origineel niet. Met een 
    // & achter het type wel (pass by reference).

    delete[] data.chromosomes;
    delete[] data.fitnessValues;
    delete[] indicesOfSurvivors;
    delete[] counterSelectedChromosomes;
}
