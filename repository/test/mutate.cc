#include "../src/library.cc"
#include "../src/utils.cc"
#include <iostream>
#include <chrono>
#include <fstream>

void correctness(std::ofstream& results, int n)
{
    std::uint64_t* bitstrings = new std::uint64_t[4 * 1000000];
    
    /* Initialize with all ones. */
    for (int i = 0; i < 4 * 1000000; i++) {
        bitstrings[i] = 0;
    }

    Vec4uq state = seedWithPcg(34959034590345);

    mutate(bitstrings, n, 4 * 1000000, state);

    /* About one in 2^-n bits should have been flipped, so be one. */
    int ones = 0;
    for (int i = 0; i < 4 * 1000000; i++) {
        ones += countOnes(bitstrings[i]);
    }
    double averageOnes = (double) ones / (4 * 1000000 * 64);

    results << "For flip probability 2^-" << n << " we have " << averageOnes <<
     " ones. This should be roughly " << (double) 1 / (1 << n) << std::endl;
    
    delete[] bitstrings;
}

std::uint64_t timings(std::ofstream& results, int n)
{
    Vec4uq state = seedWithPcg(34959034590345);

    /* One gigibyte */
    std::uint64_t totalBits = (std::uint64_t) 1024 * 1024 * 1024 * 8;
    std::uint64_t* bitstrings = new std::uint64_t[totalBits / 64];

    for (unsigned int i = 0; i < totalBits / 64; i++) {
        bitstrings[i] = 0;
    }
    
    std::chrono::high_resolution_clock::time_point time1 = 
        std::chrono::high_resolution_clock::now();

    mutate(bitstrings, n, totalBits / 64, state);

    std::chrono::high_resolution_clock::time_point time2 = 
        std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_span = 
        std::chrono::duration_cast<std::chrono::duration<double>>(time2 - time1);

    results << "Mutating one gigibyte with probability 2^-" << 
    n << " took " << time_span.count() * 1000 << " ms." << std::endl << 
    "That is " << time_span.count() * 1000000000 / (totalBits / 256) <<
    " ns. per 256 bits." << std::endl;

    std::uint64_t antiOptimisation = bitstrings[0];

    delete[] bitstrings;

    return antiOptimisation; 
}

/* Mutates a small array many times. */
std::uint64_t timingsSmall(std::ofstream& results, int n)
{
    Vec4uq state = seedWithPcg(34959034590345);

    /* One gigibyte */
    std::uint64_t totalBits = (std::uint64_t) 1024 * 8;
    std::uint64_t* bitstrings = new std::uint64_t[totalBits / 64];

    for (unsigned int i = 0; i < totalBits / 64; i++) {
        bitstrings[i] = 0;
    }

    int NITERS = 1024 * 1024;    

    std::chrono::high_resolution_clock::time_point time1 = 
        std::chrono::high_resolution_clock::now();
    
    for (int i = 0; i < NITERS; i++) {
        mutate(bitstrings, n, totalBits / 64, state);
    }

    std::chrono::high_resolution_clock::time_point time2 = 
        std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_span = 
        std::chrono::duration_cast<std::chrono::duration<double>>(time2 - time1);

    results << "Mutating one kilibyte 1024^2 times with probability 2^-" << 
    n << " took " << time_span.count() * 1000 << " ms." << std::endl << 
    "That is " << time_span.count() * 1000000000 / (totalBits * 1024 * 1024 / 256) <<
    " ns. per 256 bits." << std::endl;

    std::uint64_t antiOptimisation = bitstrings[0];

    delete[] bitstrings;

    return antiOptimisation;
}

int main()
{
    std::ofstream correctFile, timingsFile;

    correctFile.open("../results/mutateCorrectness.txt");
    timingsFile.open("../results/mutateTimings.txt");

    for (int n = 1; n <= 10; n++) {
        timings(timingsFile, n);
        correctness(correctFile, n);
        timingsSmall(timingsFile, n);
    }

    correctFile.close();
    timingsFile.close();
}
