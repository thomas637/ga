#include "../include/knapsack.hpp"
#include <iostream>

#define N 128 

int main()
{
    /* We create knapsacks of 64 items, with the ith item from the right
       costing 2^i units for i < 16, 0 else.
       Weights are 0, so objective value of a
       chromosome is just the integer value of the right-most 16 bits. */
    struct KnapsackProblem problem;
    problem.weights = new double[24];
    problem.prices = new double[24];
    problem.weightLimit = 1.0;
    problem.numberOfItems = 24;

    int chromosomesPerBin = 64 / 24;
    int numberOfBins = N / chromosomesPerBin;

    for (int i = 0; i < 16; i++) {
        problem.weights[23 - i] = 0;
        problem.prices[23 - i] = (1 << i);
    }

    for (int i = 16; i < 24; i++) {
        problem.weights[23 - i] = 0;
        problem.prices[23 - i] = 0;
    }

    std::uint64_t* knapsacks = new std::uint64_t[numberOfBins];
    double* objectiveValues = new double[N];

    /* Let the nth knapsack be n in binary. */
    for (std::uint64_t i = 0; i < N; i += 2) {
        knapsacks[i / 2] = (i << (64 - 24)) | ((i + 1) << (64 - 48));        
    }

    /* Chromosome i should have weight i. */
    fill(problem, knapsacks, objectiveValues, 24, numberOfBins);

    for (int i = 0; i < N; i++) {
        std::cout << objectiveValues[i] << " = " << permutedIndex(i, 24) << " ?" << std::endl;
    }

    delete[] knapsacks;
    delete[] objectiveValues;
    delete[] problem.prices;
    delete[] problem.weights;
}
