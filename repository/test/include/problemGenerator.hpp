#pragma once

/* This is a header only library which can be used by test programs
   to initialize a knapsack problem (as a struct defined in the top-level
   include directory). You can either make a non-trivial example with 24
   items, or there are two examples which work on any number of items, 
   with two local maxima each. */


/* 24 items, 
   from https://people.math.sc.edu/Burkardt/datasets/knapsack_01/knapsack_01.html */
struct KnapsackProblem internetProblem()
{
    struct KnapsackProblem problem;
    
    problem.weightLimit = 6404180;
    problem.numberOfItems = 24;

    problem.weights = new double[24] {382745, 799601, 909247, 729069, 467902, 44328,
        34610, 698150, 823460, 903959, 853665, 551830, 610856, 670702,
        488960, 951111, 323046, 446298, 931161, 31385, 496951, 264724,
        224916, 169684};
    problem.prices = new double[24] {825594, 1677009, 1676628, 1523970, 943972, 97426,
        69666, 1296457, 1679693, 1902996, 1844992, 1049289, 1252836, 1319836,
        953277, 2067538, 675367, 853655, 1826027, 65731, 901489, 577243,
        466257, 369261};

    return problem;
}

/* Creates numberOfItems - 1 small items and one large one. Either all the small 
   ones, or only the large one fit in the sack. The small ones are better. */
struct KnapsackProblem smallProblem(int numberOfItems)
{
    struct KnapsackProblem problem;
    
    problem.weights = new double[numberOfItems];
    problem.prices = new double[numberOfItems];

    /* Optimal solution is everything except the first item, with
       optimal value numberOfItems - 1. */
    problem.weightLimit = numberOfItems - 1;
    problem.numberOfItems = numberOfItems;

    problem.weights[0] = numberOfItems - 1;
    problem.prices[0] = numberOfItems - 2;

    for (int i = 1; i < numberOfItems; i++) {
        problem.weights[i] = 1;
        problem.prices[i] = 1;
    }

    return problem;
}

/* Creates numberOfItems - 1 small items and one large one. Either all the small 
   ones, or only the large one fit in the sack. The large one is better. */
struct KnapsackProblem largeProblem(int numberOfItems)
{
    struct KnapsackProblem problem;
    
    problem.weights = new double[numberOfItems];
    problem.prices = new double[numberOfItems];

    /* Optimal solution is the first item, with
       optimal value numberOfItems. */
    problem.weightLimit = numberOfItems - 1;
    problem.numberOfItems = numberOfItems;

    problem.weights[0] = numberOfItems - 1;
    problem.prices[0] = numberOfItems;

    for (int i = 1; i < numberOfItems; i++) {
        problem.weights[i] = 1;
        problem.prices[i] = 1;
    }

    return problem;
}

void destroyProblem(struct KnapsackProblem problem)
{
    delete[] problem.weights;
    delete[] problem.prices;
}
