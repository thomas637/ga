//
// Created by fleur on 20-1-2022.
//

#include <iostream>
#include <bitset>
#include "../include/utils.hpp"

void allSame(std::uint64_t bitstring_1, std::uint64_t bitstring_2){
    // in unionbitstring, all bits that are the same in string 1 and 2 are set to 1
    std::uint64_t unionbitstring = (bitstring_1 & bitstring_2) | (~(bitstring_1) & ~(bitstring_2));

    int count = 0;

    for(int i = 0; i < 64; i++)
    {
        //check, if there is any SET/HIGH bit
        if(~(unionbitstring) & (1<<i)) // there is a bit that is not the same in 1 and 2
        {
            count = 1;
            break;
        }
    }
    if(count == 0)
    {
        std::cout << "Gray code is correct :) " << std::endl;
        std::cout << " " << std::endl;
    }
    else
    {
        std::cout << "Gray code is incorrect :( " << std::endl;
        std::cout << " " << std::endl;
    }
}

int main()
{
    std::uint64_t all_ones = std::uint64_t(UINT64_MAX);
    std::uint64_t all_zeroes = std::uint64_t();
    std::uint64_t interchanging = 0b0101010101010101010101010101010101010101010101010101010101010101u;

    // Binary Code all ones:
    std::cout << "Binary number: ";
    printBits(all_ones);
    std::uint64_t all_ones_gray = grayCodeBinarySwitch(all_ones);
    std::cout << "Gray code that was found: ";
    printBits(all_ones_gray);
    allSame(all_ones_gray, all_ones);

    // Binary Code all zeroes:
    std::cout << "Binary number: ";
    printBits(all_zeroes);
    std::uint64_t all_zeroes_gray = grayCodeBinarySwitch(all_zeroes);
    std::cout << "Gray code that was found: ";
    printBits(all_zeroes_gray);
    allSame(all_zeroes_gray, 0b0111111111111111111111111111111111111111111111111111111111111111u);

    // Binary Code interchanging:
    std::cout << "Binary number: ";
    printBits(interchanging);
    std::uint64_t interchanging_gray = grayCodeBinarySwitch(interchanging);
    std::cout << "Gray code that was found: ";
    printBits(interchanging_gray);
    allSame(interchanging_gray, all_zeroes);

}
