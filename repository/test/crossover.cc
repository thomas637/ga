#include "../src/library.cc"
#include "../src/utils.cc"
#include <iostream>
#include <chrono>
#include <fstream>

void correctness()
{
    int numberOfBins = (1 << 20);

    std::uint64_t* bitstrings = new std::uint64_t[numberOfBins];
    
    std::uint64_t stateSingle = 234900324902;
    Vec4uq state = seedWithPcg(34959034590345);

    /* Random initialization. */
    for (int i = 0; i < numberOfBins; i += 2) {
        bitstrings[i] = xorshift(stateSingle); 
        bitstrings[i + 1] = xorshift(stateSingle); 
    }

    /* The parents are one with probability 1/2,
       so about 1/2 should be one after crossover. */
    crossoverInPlace(bitstrings, numberOfBins, state);

    int ones = 0;
    for (int i = 0; i < numberOfBins; i++) {
        ones += countOnes(bitstrings[i]);
    }
    double averageOnes = (double) ones / (numberOfBins * 64);

    std::cout << "Fraction of ones is " << averageOnes << " should be about 1/2" << std::endl;

    /* Only ones, so only ones after crossover. */
    for (int i = 0; i < numberOfBins; i++ ) {
        bitstrings[i] = 0xFFFFFFFFFFFFFFFFu; 
    }

    crossoverInPlace(bitstrings, numberOfBins, state);

    ones = 0;
    for (int i = 0; i < numberOfBins; i++) {
        ones += countOnes(bitstrings[i]);
    }

    averageOnes = (double) ones / (numberOfBins * 64);

    std::cout << "Fraction of ones is " << averageOnes << " should be 1" << std::endl;
    
    delete[] bitstrings;
}

/* Mutates a small array many times. */
std::uint64_t timingsSmall(int NITERS)
{
    Vec4uq state = seedWithPcg(34959034590345);
    std::uint64_t stateSingle = 2934020349;

    /* One gigibyte */
    std::uint64_t totalBits = (std::uint64_t) 1024 * 8;
    std::uint64_t* bitstrings = new std::uint64_t[totalBits / 64];

    for (unsigned int i = 0; i < totalBits / 64; i++) {
        bitstrings[i] = xorshift(stateSingle);
    }

    std::chrono::high_resolution_clock::time_point time1 = 
        std::chrono::high_resolution_clock::now();
    
    for (int i = 0; i < NITERS; i++) {
        crossoverInPlace(bitstrings, totalBits / 64, state);
    }

    std::chrono::high_resolution_clock::time_point time2 = 
        std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> time_span = 
        std::chrono::duration_cast<std::chrono::duration<double>>(time2 - time1);

    std::cout << "Crossing over one kilibyte " << NITERS << " times took " << time_span.count() * 1000 << " ms." << std::endl << 
    "That is " << time_span.count() * 1000000000 / (totalBits * NITERS / 256) <<
    " ns. per 256 bits." << std::endl;

    std::uint64_t antiOptimisation = bitstrings[0];

    delete[] bitstrings;

    return antiOptimisation;
}

int main()
{
    correctness();
    std::cout << toString(timingsSmall(10000000)) << " (anti-optimisation number)" << std::endl;
}

/* https://godbolt.org/z/WP9dn34o8
So 15 operations per crossover of 256 bits with 256 bits.
We have four ALUs, 3 GHz, so maximum number of operations per nanosecond is 12.
*/
