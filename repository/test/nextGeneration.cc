#include "../include/library.hpp"

/* Generates survivors (with reselection) by taking 'numberOfSurvivors' individuals, with
 * probability proportional to its fitness. We assume numberOfSurvivors is a multiple of
 * the number of chromosomes fitting into a bin. */
void selectProportionalIndices(struct Data data, struct Parameters param,
     int numberOfSurvivors, std::uint64_t& state, int* survivors) {


    // get the fitness values of the chromosomes and calculate sum
    double totalFitness = 0;
    for (int i = 0; i < param.size; ++i) {
        totalFitness += data.fitnessValues[i];
    }

    // get the index of numberOfSurvivors Survivors
    for (int i = 0; i < numberOfSurvivors; ++i) {
        double u = xorshiftDouble(state) * totalFitness;
        double accumulatedFitness = 0;
        int selected = 0;

        for (int j = 0; j < param.size; ++j) {
            double rightAccFitness = accumulatedFitness + data.fitnessValues[j];
            if (accumulatedFitness < u and u < rightAccFitness) // found chromosome
            {
                selected = j;
                break;
            } else {
                accumulatedFitness = rightAccFitness;
            }
        }
        survivors[i] = selected;
    }
}

/* Generates survivors (with reselection) by taking 'tournamentSize' random individuals,
   and passing along the fittest. We assume numberOfSurvivors is a multiple of the number
   of chromosomes fitting into a bin. */
void selectIndicesTournament(struct Data data, int numberOfSurvivors,
                      int tournamentSize, std::uint64_t& state, struct Parameters param, int* selected)
{
    for (int i = 0; i < numberOfSurvivors; ++i) {

        /* Start of tournament for the jth spot. */
        int randomIndex = xorshiftDouble(state) * param.size;
        double max = data.fitnessValues[randomIndex];
        double argmax = randomIndex;

        /* Check whether the next participant beats the previous one. */
        for (int j = 1; j < tournamentSize; j++) {
            randomIndex = xorshiftDouble(state) * param.size;
            if (data.fitnessValues[randomIndex] > max) {
                max = data.fitnessValues[randomIndex];
                argmax = randomIndex;
            }
        }
        selected[i] = argmax;
    }
}

int main()
{
    int numberOfSurvivors = 1;

    Data data;
    data.chromosomes = new std::uint64_t[1];
    data.fitnessValues = new double[6];

    double* counterSelectedChromosomes = new double[6];
    int* indicesOfSurvivors = new int[numberOfSurvivors];

    std::uint64_t state = 34959034590345;

    // create mini pool of 4 non-random chromosomes of size 4
    // let fitness function f be f(x) = sum of ones
    std::cout << "Initializing test population" << std::endl;
    std::cout << " " << std::endl;


//    chrom_1 = [1, 1, 1]; // int = 7 // fitness: 3
//    chrom_2 = [1, 1, 0]; // int = 6 // fitness: 2
//    chrom_3 = [0, 1, 1]; // int = 3 // fitness: 2
//    chrom_4 = [1, 0, 0]; // int = 4 // fitness: 1
//    chrom_5 = [0, 0, 1]; // int = 1 // fitness: 1
//    chrom_6 = [0, 0, 0]; // int = 0 // fitness: 0

    (data.fitnessValues)[0] = 3.0;
    (data.fitnessValues)[1] = 2.0;
    (data.fitnessValues)[2] = 2.0;
    (data.fitnessValues)[3] = 1.0;
    (data.fitnessValues)[4] = 1.0;
    (data.fitnessValues)[5] = 0.0;

    // int64_t of this data looks like: 1111100111000010000000000000000000000000000000000000000000000000
    // corresponding int is 17996947060925923328
    (data.chromosomes)[0] = 17996947060925923328u;

    Parameters param;
    param.bitsPerChromosome = 3;
    param.size = 6;
    param.survivorFraction = 6;
    param.state = state;

    std::cout << "Testing printChromosomes function..." << std::endl;
    std::cout << " " << std::endl;

    printChromosomes(data, param);
    std::cout << " " << std::endl;

    // do selection a lot of times; count the number of times each chromosome is chosen
    // check whether this is according to expectation

    /////////////////////////////////////////////////////////////////
    ////////////// FITNESS PROPORTIONAL SELECTION ///////////////////
    /////////////////////////////////////////////////////////////////

    // expected probabilities of being selected:
    // chrom_1 // 1/3
    // chrom_2 // 2/9
    // chrom_3 // 2/9
    // chrom_4 // 1/9
    // chrom_5 // 1/9
    // chrom_6 // 0

    std::cout << "Testing Fitness Proportional Selection..." << std::endl;
    std::cout << " " << std::endl;

    for (int j = 0; j < 6; ++j){
        counterSelectedChromosomes[j] = 0;
    }

    int indexOfSurvivor = 0;

    for (int i = 0; i < 100000; ++i){
        selectProportionalIndices(data, param, 1, state, indicesOfSurvivors);
        indexOfSurvivor = indicesOfSurvivors[0];
        counterSelectedChromosomes[indexOfSurvivor] += 1;
    }

    std::cout << "Chromosome 1: selected " << counterSelectedChromosomes[0]/1000.0 << "% of the time, ";
    std::cout << "expected 33.333%" << std::endl;

    std::cout << "Chromosome 2: selected " << counterSelectedChromosomes[1]/1000.0 << "% of the time, ";
    std::cout << "expected 22.222%" << std::endl;

    std::cout << "Chromosome 3: selected " << counterSelectedChromosomes[2]/1000.0 << "% of the time, ";
    std::cout << "expected 22.222%" << std::endl;

    std::cout << "Chromosome 4: selected " << counterSelectedChromosomes[3]/1000.0 << "% of the time, ";
    std::cout << "expected 11.111%" << std::endl;

    std::cout << "Chromosome 5: selected " << counterSelectedChromosomes[4]/1000.0 << "% of the time, ";
    std::cout << "expected 11.111%" << std::endl;

    std::cout << "Chromosome 6: selected " << counterSelectedChromosomes[5]/1000.0 << "% of the time, ";
    std::cout << "expected 0.0%" << std::endl;

    std::cout << "Finished testing proportional Selection" << std::endl;
    std::cout << " " << std::endl;


    ///////////////////////////////////////////////////////
    ////////////// TOURNAMENT SELECTION ///////////////////
    ///////////////////////////////////////////////////////

    // expected probabilities of being selected:
    // k = 2, n = 6
    // chrom_1 // m = 0 // 11/36
    // chrom_2 // m = 1 // 8/36
    // chrom_3 // m = 1 // 8/36
    // chrom_4 // m = 3 // 4/36
    // chrom_5 // m = 3 // 4/36
    // chrom_6 // m = 5 // 1/36

    std::cout << "Testing Tournament Selection" << std::endl;
    std::cout << " " << std::endl;

    for (int j = 0; j < 6; ++j){
        counterSelectedChromosomes[j] = 0;
    }

    indexOfSurvivor = 0;

    for (int i = 0; i < 100000; ++i){
        selectIndicesTournament(data, numberOfSurvivors, 2, state, param, 
            indicesOfSurvivors);
        indexOfSurvivor = indicesOfSurvivors[0];
        counterSelectedChromosomes[indexOfSurvivor] += 1;
    }

    std::cout << "Chromosome 1: selected " << counterSelectedChromosomes[0]/1000.0 << "% of the time, ";
    std::cout << "expected " << 11.0/0.360 << "%" << std::endl;

    std::cout << "Chromosome 2: selected " << counterSelectedChromosomes[1]/1000.0 << "% of the time, ";
    std::cout << "expected " << 8.0/0.360 << "%" << std::endl;

    std::cout << "Chromosome 3: selected " << counterSelectedChromosomes[2]/1000.0 << "% of the time, ";
    std::cout << "expected " << 8.0/0.360 << "%" << std::endl;

    std::cout << "Chromosome 4: selected " << counterSelectedChromosomes[3]/1000.0 << "% of the time, ";
    std::cout << "expected " << 4.0/0.360 << "%" << std::endl;

    std::cout << "Chromosome 5: selected " << counterSelectedChromosomes[4]/1000.0 << "% of the time, ";
    std::cout << "expected " << 4.0/0.360 << "%" << std::endl;

    std::cout << "Chromosome 6: selected " << counterSelectedChromosomes[5]/1000.0 << "% of the time, ";
    std::cout << "expected " << 1.0 /0.360 << "%" << std::endl;

    std::cout << "Finished testing proportional Selection" << std::endl;
    std::cout << " " << std::endl;

    delete[] data.chromosomes;
    delete[] data.fitnessValues;
    delete[] counterSelectedChromosomes; 
    delete[] indicesOfSurvivors;
}
